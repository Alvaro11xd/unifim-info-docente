<!-- CONTENIDO PRINCIPAL -->
<main id="main" class="login_main">
    <section class="contendor_login">
        <h1 class="texto_mayuscula">intranet earpfim</h1>
        <h3 class="fw-400">Sistema de Planificación de Recursos Académicos y Empresariales</h3>
        <article class="login">
            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('login') }}" id="formLogin">
                @csrf
                <div class="login_grupo_inputs">
                    <div class="login_input">
                        <input type="text" name="usuario" id="usuario" placeholder=" ">
                        <label for="usuario" class="texto_capitalize">usuario</label>
                    </div>
                    <div class="login_input">
                        <input type="password" name="contraseña" id="contraseña" placeholder=" ">
                        <label for="contraseña" class="texto_capitalize">contraseña</label>
                    </div>
                    <button class="boton login_boton texto_mayuscula">
                        iniciar sesion
                    </button>
                </div>
            </form>
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="login_recuperar_contraseña">¿Olvidó su contraseña?</a>
            @endif
            <div class="login_separador"></div>
            <div class="login_grupo_columan">
                <a href="#" class="boton boton_google">
                    <img src="assets/img/icono_google.svg" alt="icono_google">
                    <span>Iniciar sesión con Google</span>
                </a>
                <small>Temporalmente solo para docentes</small>
            </div>
            <button class="login_comunicados texto_mayuscula">
                ver comunicados
            </button>
        </article>
    </section>
</main>
