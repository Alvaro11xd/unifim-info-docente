<div class="my-4 contenedor_formulario">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Usuarios</legend>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <div class="flex justify-end">
                <x-button wire:click="crear" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo registro') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Apellido Paterno</th>
                            <th class="texto_capitalize">Apellido Materno</th>
                            <th class="texto_capitalize">Nombres</th>
                            <th class="texto_capitalize">Fecha de Nacimiento</th>
                            <th class="texto_capitalize">Estado</th>
                            <th class="texto_capitalize">Usuario</th>
                            <th class="texto_capitalize">Email</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usuarios as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->apePaterno }}</td>
                                <td>{{ $item->apeMaterno }}</td>
                                <td>{{ $item->nombres }}</td>
                                <td>{{ $item->fecNacimiento }}</td>
                                <td>{{ $item->estado }}</td>
                                <td>{{ $item->usuario }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    <x-secondary-button wire:click="editar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="abrirModalEliminar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model="modalEliminar">
                <x-slot name="title">
                    {{ __('Eliminar registro') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el registro?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalEliminar', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="eliminar ({{ $id_usuario }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model="modalCrear">
                <x-slot name="title">
                    {{ $modalTitulo }}
                </x-slot>

                <x-slot name="content">
                    <!-- Apellido Paterno -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="apePaterno" value="{{ __('Apellido Paterno') }}" />
                        <x-input id="apePaterno" type="text" class="mt-1 block w-full" wire:model="apePaterno"
                            required />
                        <x-input-error for="apePaterno" class="mt-2" />
                    </div>
                    <!-- Apellido Materno -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="apeMaterno" value="{{ __('Apellido Materno') }}" />
                        <x-input id="apeMaterno" type="text" class="mt-1 block w-full" wire:model="apeMaterno"
                            required />
                        <x-input-error for="apeMaterno" class="mt-2" />
                    </div>
                    <!-- Nombres -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="nombres" value="{{ __('Nombres') }}" />
                        <x-input id="nombres" type="text" class="mt-1 block w-full" wire:model="nombres"
                            required />
                        <x-input-error for="nombres" class="mt-2" />
                    </div>
                    <!-- Fecha de Nacimiento -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="fecNacimiento" value="{{ __('Fecha de Nacimiento') }}" />
                        <x-input id="fecNacimiento" type="text" class="mt-1 block w-full" wire:model="fecNacimiento"
                            required placeholder='YY-MM-dd' />
                        <x-input-error for="fecNacimiento" class="mt-2" />
                    </div>
                    <!-- Usuario -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="usuario" value="{{ __('Usuario') }}" />
                        <x-input id="usuario" type="text" class="mt-1 block w-full" wire:model="usuario"
                            required />
                        <x-input-error for="usuario" class="mt-2" />
                    </div>
                    <!-- Correo electrónico -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="email" value="{{ __('Correo electrónico') }}" />
                        <x-input id="email" type="text" class="mt-1 block w-full" wire:model="email" required />
                        <x-input-error for="email" class="mt-2" />
                    </div>
                    <!-- Contraseña -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="password" value="{{ __('Contraseña') }}" />
                        <x-input id="password" type="text" class="mt-1 block w-full" wire:model="password"
                            required />
                        <x-input-error for="password" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalCrear', true)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="guardar" wire:loading.attr="disabled">
                        {{ $botonTitulo }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
