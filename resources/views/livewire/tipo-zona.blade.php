<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Tipos de Zona disponibles</legend>
            <div class="flex justify-end">
                <x-button wire:click="crear" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo registro') }}
                </x-button>
            </div>
            <!-- TABLA -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">Id</th>
                            <th class="texto_capitalize">Zona</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tiposZona as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->zona }}</td>
                                <td>
                                    <x-secondary-button wire:click="editar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="abrirModalEliminar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model="modalEliminar">
                <x-slot name="title">
                    {{ __('Eliminar registro') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el registro?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalEliminar', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="eliminar ({{ $id_zona }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model="modalCrear">
                <x-slot name="title">
                    {{ $modalTitulo }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="zona" value="{{ __('Zona') }}" />
                        <x-input id="zona" type="text" class="mt-1 block w-full" wire:model="zona" required />
                        <x-input-error for="zona" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalCrear', true)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="guardar" wire:loading.attr="disabled">
                        {{ $botonTitulo }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
