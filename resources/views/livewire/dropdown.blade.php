<div class="grupos_inputs">
    <div class="grupo grupo-1">
        <div class="grupo_fila grupo_fila-100">
            <label for="pais">país</label>
            <div class="select">
                <select name="pais" id="pais" wire:model.live="selectedPais">
                    <option value="" selected>Seleccionar</option>
                    @foreach ($paises as $item)
                        <option value="{{ $item->id }}">{{ $item->pais }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="grupo_fila grupo_fila-100">
            <label for="provincia">provincia</label>
            <div class="select">
                <select name="provincia" id="provincia" wire:model.live="selectedProvincia">
                    <option>Debe seleccionar un DEPARTAMENTO antes</option>
                    @if (!is_null($provincias))
                        @foreach ($provincias as $provincia)
                            <option value="{{ $provincia->id }}">{{ $provincia->provincia }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="grupo grupo-2">
        <div class="grupo_fila grupo_fila-100">
            <label for="departamento">departamento</label>
            <div class="select">
                <select name="departamento" id="departamento" wire:model.live="selectedDepartamento">
                    <option>Debe seleccionar un PAIS antes</option>
                    @if (!is_null($departamentos))
                        @foreach ($departamentos as $departamento)
                            <option value="{{ $departamento->id }}">{{ $departamento->departamento }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="grupo_fila grupo_fila-100">
            <label for="distrito">distrito</label>
            <div class="select">
                <select name="distrito" id="distrito">
                    <option>Debe seleccionar una PROVINCIA antes</option>
                    @if (!is_null($distritos))
                        @foreach ($distritos as $distrito)
                            <option value="{{ $distrito->id }}">{{ $distrito->distrito }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
</div>
