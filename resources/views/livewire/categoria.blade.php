<div class="my-4 contenedor_formulario">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Categoría laboral de los docentes</legend>
            <div class="flex justify-end">
                <x-button wire:click="confirmCategoriaAdd" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nueva categoría') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Categoria</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categorias as $item)
                            <tr>
                                <td>{{ $contador }}</td>
                                <td>{{ $item->categoria }}</td>
                                <td>
                                    <x-secondary-button wire:click="confirmCategoriaEdit ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="confirmCategoriaDeletion ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="confirmingCategoriaDeletion">
                <x-slot name="title">
                    {{ __('Eliminar categoria') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar la categoria?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingCategoriaDeletion', false)"
                        wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="deleteCategoria ({{ $confirmingCategoriaDeletion }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="confirmingCategoriaAdd">
                <x-slot name="title">
                    {{ isset($this->categoriaModel->id) ? 'Editar categoria' : 'Crear categoria' }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="categoriaModel.categoria" value="{{ __('Categoria') }}" />
                        <x-input id="categoriaModel.categoria" type="text" class="mt-1 block w-full" wire:model="categoria" required />
                        <x-input-error for="categoria" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingCategoriaAdd', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="saveCategoria" wire:loading.attr="disabled">
                        {{ isset($this->categoriaModel->id) ? 'Actualizar' : 'Guardar' }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
