<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Paises</legend>
            <div class="flex justify-end">
                <x-button wire:click="confirmPaisAdd" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo pais') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">Id</th>
                            <th class="texto_capitalize">Pais</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paises as $item)
                            <tr>
                                <td>{{ $contador }}</td>
                                <td>{{ $item->pais }}</td>
                                <td>
                                    <x-secondary-button wire:click="confirmPaisEdit ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="confirmPaisDeletion ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="confirmingPaisDeletion">
                <x-slot name="title">
                    {{ __('Eliminar pais') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el Pais?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingPaisDeletion', false)"
                        wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="deletePais ({{ $confirmingPaisDeletion }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="confirmingPaisAdd">
                <x-slot name="title">
                    {{ isset($this->paisModel->id) ? 'Editar pais' : 'Crear pais' }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="paisModel.pais" value="{{ __('Pais') }}" />
                        <x-input id="paisModel.pais" type="text" class="mt-1 block w-full" wire:model="pais"
                            required />
                        <x-input-error for="pais" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingPaisAdd', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="savePais" wire:loading.attr="disabled">
                        {{ isset($this->paisModel->id) ? 'Actualizar' : 'Guardar' }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
