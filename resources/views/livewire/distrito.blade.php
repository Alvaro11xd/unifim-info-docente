<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Distritos</legend>
            <div class="flex justify-end">
                <x-button wire:click="crear" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo distrito') }}
                </x-button>
            </div>
            <!-- TABLA -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Distrito</th>
                            <th class="texto_capitalize">Provincia</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($distritos as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->distrito }}</td>
                                <td>{{ $item->provincia->provincia }}</td>
                                <td>
                                    <x-secondary-button wire:click="editar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="modalEliminar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $distritos->links() }}
                </div>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="idEliminar">
                <x-slot name="title">
                    {{ __('Eliminar distrito') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar la Distrito?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('idEliminar', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="eliminar ({{ $idEliminar }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="modalCrear">
                <x-slot name="title">
                    {{ $modalTitulo }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="distritoModel.distrito" value="{{ __('Distrito') }}" />
                        <x-input id="distritoModel.distrito" type="text" class="mt-1 block w-full"
                            wire:model="distrito" required />
                        <x-input-error for="distrito" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <label class="block text-sm font-medium text-gray-700">
                            Selecciona una provincia
                        </label>
                        <select wire:model.live="idProvincia"
                            class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option>Seleccionar</option>
                            @foreach ($provincias as $provincia)
                                <option value="{{ $provincia->id }}"
                                    {{ $idProvincia == $provincia->id ? 'selected' : '' }}>
                                    {{ $provincia->provincia }}</option>
                            @endforeach
                        </select>
                        <x-input-error for="idProvincia" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalCrear', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="guardar" wire:loading.attr="disabled">
                        {{ $botonTitulo }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
