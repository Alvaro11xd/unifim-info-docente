<!-- BARRA DE NAVEGACION -->
<nav id="nav" class="nav">
    <div class="nav_menu nav_menu-center">
        <img src="assets/img/logo_uni_2.png" style="height: 50px;" alt="logo_uni">
        <div class="texto_escritorio texto_mayuscula fw-400 font_mediana d-none">
            universidad nacional de ingeniería
            <br>
            facultad de ingeniería mecánica
        </div>
        <div class="texto_movil texto_mayuscula fw-700 font_grande">
            uni fim
        </div>
    </div>
</nav>
