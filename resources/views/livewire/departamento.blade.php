<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Departamento</legend>
            <div class="flex justify-end">
                <x-button wire:click="confirmDepartamentoAdd" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo departamento') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Departamento</th>
                            <th class="texto_capitalize">Pais</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($departamentos as $item)
                            <tr>
                                <td>{{ $contador }}</td>
                                <td>{{ $item->departamento }}</td>
                                <td>{{ $item->pais->pais }}</td>
                                <td>
                                    <x-secondary-button wire:click="confirmDepartamentoEdit ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="confirmDepartamentoDeletion ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $departamentos->links() }}
                </div>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="confirmingDepartamentoDeletion">
                <x-slot name="title">
                    {{ __('Eliminar departamento') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el Departamento?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingDepartamentoDeletion', false)"
                        wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3"
                        wire:click="deleteDepartamento ({{ $confirmingDepartamentoDeletion }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="confirmingDepartamentoAdd">
                <x-slot name="title">
                    {{ isset($this->departamentoModel->id) ? 'Editar departamento' : 'Crear departamento' }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="departamentoModel.departamento" value="{{ __('Departamento') }}" />
                        <x-input id="departamentoModel.departamento" type="text" class="mt-1 block w-full"
                            wire:model="departamento" required />
                        <x-input-error for="departamento" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <label class="block text-sm font-medium text-gray-700">
                            Selecciona un país
                        </label>
                        <select wire:model.live="paisId"
                            class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                             <option>Seleccionar</option>
                            @foreach ($paises as $pais)
                                <option value="{{ $pais->id }}" {{ $paisId == $pais->id ? 'selected' : '' }}>{{ $pais->pais }}</option>
                            @endforeach
                        </select>
                        <x-input-error for="paisId" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingDepartamentoAdd', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="saveDepartamento" wire:loading.attr="disabled">
                        {{ isset($this->departamentoModel->id) ? 'Actualizar' : 'Guardar' }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
