<div class="contenedor_formulario">
    <div class="contenedor_perfil">
        <div class="perfil_usuario">
            <img src="{{ Auth()->user()->profile_photo_url }}" alt="foto_docente" id="fotoDocente">
            <div class="perfil_usuario_contenido">
                <div class="perfil_nombre">
                    <div class="texto_mayuscula">
                        <span>{{ Auth()->user()->nombres }} {{Auth()->user()->apePaterno}}</span>
                    </div>
                    <a href="#">08161078</a>
                </div>
                <div class="perfil_botones">
                    <button type="button" role="button" class="perfil_boton">
                        <legend>Actualizar</legend>
                        <div>
                            <i class="fa-solid fa-cloud-arrow-up"></i>
                            <input type="file" id="inputFoto">
                        </div>
                    </button>
                    <button type="button" role="button" id="btnModalFotos" class="perfil_boton">
                        <legend>Historial</legend>
                        <div>
                            <i class="fa-solid fa-file-lines"></i>
                            <input type="hidden">
                        </div>
                    </button>
                    <!-- VENTANA POP UP PARA MOSTRAR HISTORIAL DE FOTOS -->
                    <div class="ventana_overlay" id="modalFotos">
                        <div class="ventana_contenedor" id="modal_5">
                            <legend class="texto_capitalize">Historial de fotos</legend>
                            <section class="historial_fotos"></section>
                            <!-- BOTONES EN VENTANA MODAL -->
                            <footer class="ventana_botones">
                                <button class="ventana_btn ventana_btn-secundario"
                                    id="modal_btn_cerrar">cancelar</button>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="" method="" class="contenedor_botones_accion">
        <button>
            <i class="fa-solid fa-rotate-right"></i>
            <input type="hidden">
        </button>
        <button>
            Generar PDF
            <input type="hidden">
        </button>
    </form>
    <div class="caja_tab">
        <button class="btn_tab btn_tab-activo">1er</button>
        <button class="btn_tab">2do</button>
        <button class="btn_tab">3er</button>
        <button class="btn_tab">4to</button>
    </div>
    <div class="caja_contenido">
        <!-- DATOS PERSONALES -->
        <div class="contenido btn_tab-activo">
            <legend class="contenido_titulo texto_mayuscula">1.- datos personales</legend>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form action="{{ route('docente.datosPersonales.store') }}" method="POST">
                @csrf
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                <div class="grupos_inputs">
                    <div class="grupo grupo-1">
                        <div class="grupo_columna grupo_fila-100">
                            <div class="grupo_fila grupo_fila-100">
                                <label for="sexo">sexo</label>
                                <div class="select">
                                    <select name="sexo" id="sexo">
                                        <option value="" selected disabled>Seleccionar</option>
                                        @foreach ($sexos as $item)
                                            <option value="{{ $item->id }}">{{ $item->sexo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                <input type="date" name="fecha_nacimiento" id="fecha_nacimiento">
                            </div>
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="telefono">teléfono fijo
                                <i class="fa-solid fa-pen"></i>
                            </label>
                            <input type="tel" name="telefono" id="telefono" placeholder="5486212 | 5486212">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="ruc" class="texto_mayuscula">ruc
                            </label>
                            <input type="text" id="ruc" name="ruc">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="bc">brevete y categoría
                            </label>
                            <input type="text" id="bc" name="brevete">
                        </div>
                        <div class="grupo_columna">
                            <div class="grupo_fila grupo_fila-100">
                                <label for="estado_civil">estado civil</label>
                                <div class="select">
                                    <select name="estado_civil" id="estado_civil">
                                        <option value="" selected disabled>Seleccionar</option>
                                        @foreach ($estados as $item)
                                            <option value="{{ $item->id }}">{{ $item->civil }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <label for="fecha_matrimonio">fecha de matrimonio</label>
                                <input type="date" name="fecha_matrimonio" id="fecha_matrimonio">
                            </div>
                        </div>
                    </div>
                    <div class="grupo grupo-2">
                        <div class="grupo_fila grupo_fila-100">
                            <div class="grupo_columna grupo_fila-100">
                                <div class="grupo_fila">
                                    <label for="dni">dni</label>
                                    <input type="text" id="dni" name="dni" placeholder="05489612">
                                </div>
                                <div action="" method=""
                                    class="contenedor_botones_accion contenedor_botones_accion--2">
                                    <button>
                                        <i class="fa-solid fa-cloud-arrow-up"></i>
                                        <input type="hidden">
                                    </button>
                                    <button>
                                        <i class="fa-solid fa-eye"></i>
                                        <input type="hidden">
                                    </button>
                                    <button>
                                        <i class="fa-solid fa-trash"></i>
                                        <input type="hidden">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="celular">celular
                                <i class="fa-solid fa-pen"></i>
                            </label>
                            <input type="text" name="celular" id="celular">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="lib_militar">lib militar
                            </label>
                            <input type="text" id="lib_militar" name="lib_militar">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="vehiculo">si cuenta con vehículo, indicar modelo, placa, año, color
                            </label>
                            <input type="text" id="vehiculo" name="vehiculo">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="grupo_sanguineo">grupo sanguíneo
                            </label>
                            <input type="text" id="grupo_sanguineo" name="grupo_sanguineo">
                        </div>
                    </div>
                </div>
                <button type="submit" class="boton texto_mayuscula">añadir registro</button>
            </form>
            <!-- LUGAR DE NACIMIENTO -->
            <legend class="contenido_titulo texto_capitalize titulo_2">lugar de nacimiento</legend>
            @livewire('dropdown')
            <!-- DOMICILIO -->
            <legend class="contenido_titulo texto_capitalize titulo_2">domicilio</legend>
            <button class="boton texto_mayuscula" id="btn_añadir_domicilio" wire:click.prevent="crear">añadir
                registro</button>
            <!-- VENTANA POP UP PARA REGISTRAR UN DOMICILIO -->
            <form action="{{ route('docente.domicilios.store') }}" method="POST" class="ventana_overlay"
                id="modal_overlay_1">
                @csrf
                <div class="ventana_contenedor" id="modal_1">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <legend class="texto_capitalize">{{ $modalTitulo }}</legend>

                    <div class="grupos_inputs">
                        <div class="grupo grupo-1">
                            <div class="grupo_fila grupo_fila-100">
                                <label for="pais">país</label>
                                <div class="select">
                                    <select name="pais" id="pais" wire:model.live="selectedPais">
                                        <option value="" selected>Seleccionar</option>
                                        @foreach ($paises as $item)
                                            <option value="{{ $item->id }}">{{ $item->pais }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <label for="provincia">provincia</label>
                                <div class="select">
                                    <select name="provincia" id="provincia" wire:model.live="selectedProvincia">
                                        <option>Debe seleccionar un DEPARTAMENTO antes</option>
                                        @if (!is_null($provincias))
                                            @foreach ($provincias as $provincia)
                                                <option value="{{ $provincia->id }}">{{ $provincia->provincia }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="grupo grupo-2">
                            <div class="grupo_fila grupo_fila-100">
                                <label for="departamento">departamento</label>
                                <div class="select">
                                    <select name="departamento" id="departamento"
                                        wire:model.live="selectedDepartamento">
                                        <option>Debe seleccionar un PAIS antes</option>
                                        @if (!is_null($departamentos))
                                            @foreach ($departamentos as $departamento)
                                                <option value="{{ $departamento->id }}">
                                                    {{ $departamento->departamento }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <label for="distrito">distrito</label>
                                <div class="select">
                                    <select name="distrito" id="distrito">
                                        <option>Debe seleccionar una PROVINCIA antes</option>
                                        @if (!is_null($distritos))
                                            @foreach ($distritos as $distrito)
                                                <option value="{{ $distrito->id }}">{{ $distrito->distrito }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grupos_inputs">
                        <div class="grupo grupo-1">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="tipo_via">tipo de vía</label>
                                    <div class="select">
                                        <select name="tipo_via" id="tipo_via" wire:model.live="tipo_via">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($tiposVia as $item)
                                                <option value="{{ $item->id }}">{{ $item->via }}</option>
                                            @endforeach
                                        </select>

                                        @error('tipo_via')
                                            <small> {{ $message }} </small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="tipo">tipo</label>
                                    <div class="select">
                                        <select name="tipo" id="tipo">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($tipos as $item)
                                                <option value="{{ $item->id }}">{{ $item->tipo }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="tipo_zona">tipo de zona</label>
                                    <div class="select">
                                        <select name="tipo_zona" id="tipo_zona">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($tiposZona as $item)
                                                <option value="{{ $item->id }}">{{ $item->zona }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grupo grupo-2">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="nombre_via">nombre de la vía</label>
                                    <input type="text" name="nombre_via" id="nombre_via"
                                        placeholder="calle, avenida, plaza, etc.">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="numero_inmueble">numero de inmueble</label>
                                    <input type="number" name="numero_inmueble" id="numero_inmueble"
                                        placeholder="#000">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="nombre_zona">nombre de la zona</label>
                                    <input type="text" id="nombre_zona">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BOTONES EN VENTANA MODAL -->
                    <div class="ventana_botones">
                        <button type="submit" class="ventana_btn ventana_btn-primario">Guardar</button>
                        <button class="ventana_btn ventana_btn-secundario" id="modal_btn_cerrar">cancelar</button>
                    </div>
                </div>
            </form>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1" id="tabla_domicilio">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">orden</th>
                            <th class="texto_capitalize">país</th>
                            <th class="texto_capitalize">dirección extranjera</th>
                            <th class="texto_capitalize">departamento</th>
                            <th class="texto_capitalize">provincia</th>
                            <th class="texto_capitalize">distrito</th>
                            <th class="texto_capitalize">tipo de vía</th>
                            <th class="texto_capitalize">nombre de la vía</th>
                            <th class="texto_capitalize">tipo</th>
                            <th class="texto_capitalize">número de inmueble</th>
                            <th class="texto_capitalize">tipo de zona</th>
                            <th class="texto_capitalize">nombre de la zona</th>
                            <th colspan="2" class="texto_capitalize">opciones</th>
                        </tr>
                    </thead>
                    <tbody id="contenido_domicilio">
                        @foreach ($domicilios as $item)
                            <tr>
                                <td> {{ $item->id }} </td>
                                <td>
                                    @if ($item->pais)
                                        {{ $item->pais->pais }}
                                    @else
                                        Sin país asociado
                                    @endif
                                </td>
                                <td> - </td>
                                <td>
                                    @if ($item->pais && $item->pais->departamento->isNotEmpty())
                                        @foreach ($item->pais->departamento as $departamento)
                                            {{ $departamento->departamento }}
                                        @endforeach
                                    @else
                                        Sin departamentos asociados
                                    @endif
                                </td>

                                <td>
                                    @if ($item->pais && $item->pais->departamento->isNotEmpty())
                                        @foreach ($item->pais->departamento as $departamento)
                                            <strong>{{ $departamento->departamento }}</strong><br>

                                            @if ($departamento->provincia->isNotEmpty())
                                                @foreach ($departamento->provincia as $provincia)
                                                    {{ $provincia->provincia }}<br>
                                                @endforeach
                                            @else
                                                Sin provincias asociadas
                                            @endif
                                        @endforeach
                                    @else
                                        Sin departamentos asociados
                                    @endif
                                </td>
                                <td>
                                    @if ($item->pais && $item->pais->departamento->isNotEmpty())
                                        @foreach ($item->pais->departamento as $departamento)
                                            @if ($departamento->provincia->isNotEmpty())
                                                @foreach ($departamento->provincia as $provincia)
                                                    @if ($provincia->distrito->isNotEmpty())
                                                        @foreach ($provincia->distrito as $distrito)
                                                            {{ $distrito->distrito }}<br>
                                                        @endforeach
                                                    @else
                                                        Sin distritos asociados
                                                    @endif
                                                @endforeach
                                            @else
                                                Sin distritos asociados
                                            @endif
                                        @endforeach
                                    @else
                                        Sin departamentos asociados
                                    @endif
                                </td>


                                <td>
                                    @if ($item->tipoVia)
                                        {{ $item->tipoVia->via }}
                                    @else
                                        Sin tipo de vía asociada
                                    @endif
                                </td>
                                <td> {{ $item->nombreVia }} </td>
                                <td>
                                    @if ($item->tipo)
                                        {{ $item->tipo->tipo }}
                                    @else
                                        Sin tipo asociada
                                    @endif
                                </td>
                                <td> {{ $item->numeroInmueble }} </td>
                                <td>
                                    @if ($item->tipoZona)
                                        {{ $item->tipoZona->zona }}
                                    @else
                                        Sin zona asociada
                                    @endif
                                </td>
                                <td> {{ $item->nombreZona }} </td>
                                <!-- BOTONES DE EDITAR Y ELIMINAR -->
                                <td>
                                    <button class="boton editar">Editar <i
                                            class="fa-regular fa-pen-to-square"></i></button>
                                </td>
                                <td>
                                    <button class="boton eliminar">Eliminar <i class="fa-solid fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $domicilios->links() }}
                </div>
            </div>
            <!-- FAMILIAR -->
            <!-- PERSONAS A COMUNICAR EN CASO DE EMERGENCIA -->
            <legend class="contenido_titulo texto_capitalize titulo_2">1.1.- personas a comunnicar en caso de
                emergencia</legend>
            <div class="grupos_inputs">
                <div class="grupo grupo-1">
                    <div class="grupo_fila grupo_fila-100">
                        <span class="texto_capitalize">contacto 1 (*)</span>
                        <label for="contacto1_familiar" class="texto_capitalize">apellidos y nombres
                        </label>
                        <input type="text" id="contacto1_familiar" value="">
                    </div>
                    <div class="grupo_fila grupo_fila-100">
                        <span class="texto_capitalize">contacto 2 (*)</span>
                        <label for="contacto2_familiar" class="texto_capitalize">apellidos y nombres
                        </label>
                        <input type="text" id="contacto2_familiar" value="">
                    </div>
                    <div class="grupo_fila grupo_fila-100">
                        <span class="texto_capitalize">contacto 3 (*)</span>
                        <label for="contacto3_familiar" class="texto_capitalize">apellidos y nombres
                        </label>
                        <input type="text" id="contacto3_familiar" value="">
                    </div>
                </div>
                <div class="grupo grupo-2">
                    <div class="grupo_fila grupo_fila-100">
                        <div class="grupo_columna grupo_fila-100">
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="parentesco1_familiar" class="texto_capitalize">parentesco
                                </label>
                                <input type="text" id="parentesco1_familiar" value="">
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="celular1_familiar" class="texto_capitalize">celular
                                </label>
                                <input type="tel" id="celular1_familiar" value="">
                            </div>
                        </div>
                    </div>
                    <div class="grupo_fila grupo_fila-100">
                        <div class="grupo_columna grupo_fila-100">
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="parentesco2_familiar" class="texto_capitalize">parentesco
                                </label>
                                <input type="text" id="parentesco2_familiar" value="">
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="celular2_familiar" class="texto_capitalize">celular
                                </label>
                                <input type="tel" id="celular2_familiar" value="">
                            </div>
                        </div>
                    </div>
                    <div class="grupo_fila grupo_fila-100">
                        <div class="grupo_columna grupo_fila-100">
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="parentesco3_familiar" class="texto_capitalize">parentesco
                                </label>
                                <input type="text" id="parentesco3_familiar" value="">
                            </div>
                            <div class="grupo_fila grupo_fila-100">
                                <span class="hidden">oculto</span>
                                <label for="celular3_familiar" class="texto_capitalize">celular
                                </label>
                                <input type="tel" id="celular3_familiar" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="boton texto_mayuscula" id="btn_añadir_familiar">añadir registro</button>
            <!-- VENTANA POP UP PARA REGISTRAR UN DOMICILIO -->
            <div class="ventana_overlay" id="modal_overlay_2">
                <div class="ventana_contenedor" id="modal_2">
                    <legend class="texto_capitalize">agregar familiar</legend>
                    <div class="grupos_inputs">
                        <div class="grupo grupo-1">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="nombre_familiar">Apellidos y Nombres</label>
                                    <input type="text" name="" id="nombre_familiar">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="nacimiento_familiar">Fecha de Nacimiento</label>
                                    <input type="date" name="" id="nacimiento_familiar">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="estudios_familiar">Nivel de Estudios</label>
                                    <div class="select">
                                        <select name="" id="estudios_familiar">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($estudios as $item)
                                                <option value="{{ $item->id }}">{{ $item->estudios }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grupo grupo-2">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="parentesco">parentesco</label>
                                    <div class="select">
                                        <select name="" id="parentesco">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($parentescos as $item)
                                                <option value="{{ $item->id }}">{{ $item->parentesco }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="est_civil">est. civil</label>
                                    <div class="select">
                                        <select name="" id="est_civil">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($estados as $item)
                                                <option value="{{ $item->id }}">{{ $item->civil }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="ocupacion_familiar">ocupación</label>
                                    <input type="text" name="" id="ocupacion_familiar">
                                </div>
                            </div>
                        </div>
                        <div class="radio_contenedor">
                            <span>¿Depende económicamente de Ud.?</span>
                            <div class="radio_grupos">
                                <div class="radio_grupo">
                                    <span>Sí</span>
                                    <input type="radio" name="" id="">
                                </div>
                                <div class="radio_grupo">
                                    <span>No</span>
                                    <input type="radio" name="" id="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BOTONES EN VENTANA MODAL -->
                    <div class="ventana_botones">
                        <button class="ventana_btn ventana_btn-primario">guardar</button>
                        <button class="ventana_btn ventana_btn-secundario" id="modal_btn_cerrar">cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- DATOS LABORALES -->
        <div class="contenido">
            <legend class="contenido_titulo texto_mayuscula">2.- datos laborales</legend>
            <div class="grupos_inputs">
                <div class="grupo grupo-1">
                    <div class="grupo_columna grupo_fila-100">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="condicion_docente">condición (C/N)</label>
                            <div class="select">
                                <select name="" id="condicion_docente">
                                    <option value="" selected disabled>Seleccionar</option>
                                    @foreach ($condiciones as $item)
                                        <option value="{{ $item->id }}">{{ $item->condicion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="categoria_docente">categoría</label>
                            <div class="select">
                                <select name="" id="categoria_docente">
                                    <option value="" selected disabled>Seleccionar</option>
                                    @foreach ($categorias as $item)
                                        <option value="{{ $item->id }}">{{ $item->categoria }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="grupo_columna grupo_fila-100">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="horas_lectivas_docente">horas lectivas
                            </label>
                            <input type="text" name="" id="horas_lectivas_docente" value="16"
                                readonly>
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="dependencia_laboral_docente">Dependencia donde labora (Actual)
                            </label>
                            <input type="text" name="" id="dependencia_laboral_docente" value="">
                        </div>
                    </div>
                    <div class="grupo_columna grupo_fila-100">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="f_ingreso">fecha ingreso FIM
                            </label>
                            <input type="date" id="f_ingreso" value="">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="f_nombramiento">fecha nombramiento
                            </label>
                            <input type="date" id="f_nombramiento" value="">
                        </div>
                    </div>
                    <div class="grupo_columna">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="afp_nombre_docente">AFP (Nombre)
                            </label>
                            <input type="text" name="" id="afp_nombre_docente" value="">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="cod_afp_docente">Cód AFP
                            </label>
                            <input type="text" name="" id="cod_afp_docente" value="">
                        </div>
                    </div>
                </div>
                <div class="grupo grupo-2">
                    <div class="grupo_columna grupo_fila-100">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="dedicacion_docente">dedicación</label>
                            <div class="select">
                                <select name="" id="dedicacion_docente">
                                    <option value="" selected disabled>Seleccionar</option>
                                    @foreach ($dedicaciones as $item)
                                        <option value="{{ $item->id }}">{{ $item->dedicacion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="horas_docente">horas
                            </label>
                            <input type="text" name="" id="horas_docente" value="32" readonly>
                        </div>
                    </div>
                    <div class="grupo_columna grupo_fila-100">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="pension_docente">regimen pensionario</label>
                            <div class="select">
                                <select name="" id="pension_docente">
                                    <option value="" selected disabled>Seleccionar</option>
                                    @foreach ($regimenes as $item)
                                        <option value="{{ $item->id }}">{{ $item->regimen }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="grupo_columna">
                        <div class="grupo_fila grupo_fila-100">
                            <label for="cod_esalud_docente">Cód. EsSalud
                            </label>
                            <input type="text" name="" id="cod_esalud_docente" value="">
                        </div>
                        <div class="grupo_fila grupo_fila-100">
                            <label for="otro_seguro_docente">otro tipo de seguro
                            </label>
                            <input type="text" name="" id="otro_seguro_docente" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- DATOS DE FORMACION PROFESIONAL -->
        <div class="contenido">
            <div>
                <legend class="contenido_titulo texto_mayuscula mb-0">3.- datos de formación profesional:
                </legend>
                <span class="contenido_titulo_span">Indicar Grados y TÍtulos obtenidos, señalando Fecha de
                    Revalidación, para el caso de Títulos y Grados otorgados en el Extranjero</span>
            </div>

            <button class="boton texto_mayuscula" id="btn_añadir_formacion">añadir registro</button>
            <!-- VENTANA POP UP PARA REGISTRAR FORMACION PROFESIONAL -->
            <div class="ventana_overlay" id="modal_overlay_3">
                <div class="ventana_contenedor" id="modal_2">
                    <legend class="texto_capitalize">agregar formación profesional</legend>
                    <div class="grupos_inputs">
                        <div class="grupo grupo-1">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="formacionProfesional">Formación profesional</label>
                                    <div class="select">
                                        <select name="" id="formacionProfesional">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($formaciones as $item)
                                                <option value="{{ $item->id }}">{{ $item->formacion }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="universidad_profesional">Universidad</label>
                                    <input type="text" name="" id="universidad_profesional">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="f_inicio_profesional">Fecha Inicio
                                    </label>
                                    <input type="date" id="f_inicio_profesional" value="">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="grado_profesional">Grado obtenido</label>
                                    <input type="text" name="" id="grado_profesional">
                                </div>
                            </div>
                        </div>
                        <div class="grupo grupo-2">
                            <div class="grupo_fila-100">
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="paisProfesional">País</label>
                                    <div class="select">
                                        <select name="" id="paisProfesional">
                                            <option value="" selected disabled>Seleccionar</option>
                                            @foreach ($paises as $item)
                                                <option value="{{ $item->id }}">{{ $item->pais }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="especialidad_profesional">Especialidad</label>
                                    <input type="text" name="" id="especialidad_profesional">
                                </div>
                                <div class="grupo_fila grupo_fila-100">
                                    <label for="f_final_profesional">Fecha Final
                                    </label>
                                    <input type="date" id="f_inicio_profesional" value="">
                                </div>
                                <div class="grupo_fila grupo_fila-100 file_contenedor">
                                    <label for="f_final_profesional" class="hidden">Documento sustentario
                                    </label>
                                    <button type="button" class="file_icono">
                                        Documento sustentario
                                        <i class="fa-solid fa-cloud-arrow-up"></i>
                                        <input type="file" name="" id="pdfFile">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="grupo_fila-100 textarea_contenedor">
                            <label for="observacion_profesional">Observación</label>
                            <textarea name="" id="observacion_profesional"></textarea>
                        </div>
                    </div>
                    <!-- BOTONES EN VENTANA MODAL -->
                    <div class="ventana_botones">
                        <button class="ventana_btn ventana_btn-primario">guardar</button>
                        <button class="ventana_btn ventana_btn-secundario" id="modal_btn_cerrar">cancelar</button>
                    </div>
                </div>
            </div>

            <!-- VENTANA POP UP PARA MOSTRAR EL PDF SUSTENTATORIO -->
            <div class="ventana_overlay" id="modal_overlay_4">
                <div class="ventana_contenedor" id="modal_4">
                    <embed id="vistaPrevia" type="application/pdf" width="500" height="500">
                </div>
            </div>

            <!-- TABLA DATOS DE FORMACION PROFESIONAL -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1" id="tabla_datos_profesional">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">orden</th>
                            <th class="texto_capitalize">formación profesional</th>
                            <th class="texto_capitalize">país</th>
                            <th class="texto_capitalize">universidad</th>
                            <th class="texto_capitalize">especialidad</th>
                            <th class="texto_capitalize">fecha inicio</th>
                            <th class="texto_capitalize">fecha final</th>
                            <th class="texto_capitalize">grado obtenido</th>
                            <th class="texto_capitalize">documento sustentorio</th>
                            <th class="texto_capitalize">observaciones</th>
                            <th colspan="2" class="texto_capitalize">opciones</th>
                        </tr>
                    </thead>
                    <tbody id="contenido_datos_profesional"></tbody>
                </table>
            </div>
        </div>
        <div class="contenido">
            <div class="no_content"></div>
        </div>
    </div>
</div>
