<!-- MENU LATERAL -->
<aside id="side_menu">
    <a href="/" class="aside_logo">
        <img src="{{ asset('assets/img/logo_uni.png') }}" alt="logo_uni">
        <div class="aside_logo_textos">
            <h4 class="texto_mayuscula">uni earpfim</h4>
            <span class="texto_mayuscula">practicante</span>
        </div>
    </a>

    <ul class="list">

        <li class="list_item list_item--click">
            <div class="list_button list_button--click">
                <i class="fa-solid fa-briefcase list_img"></i>
                <a href="#" class="nav_link">General</a>
                <i class="fa-solid fa-angle-down list_arrow"></i>
            </div>

            <ul class="list_show">
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('contenido.index') }}"
                        class="nav_link nav_link--inside texto_capitalize">información docente</a>
                </li>
            </ul>
        </li>

        <li class="list_item list_item--click">
            <div class="list_button list_button--click">
                <i class="fa-solid fa-briefcase list_img"></i>
                <a href="#" class="nav_link">Administrador</a>
                <i class="fa-solid fa-angle-down list_arrow"></i>
            </div>

            <ul class="list_show">
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.usuarios') }}" class="nav_link nav_link--inside">Usuarios</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.estadoCivil') }}" class="nav_link nav_link--inside">Estado civil</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.sexo') }}" class="nav_link nav_link--inside">Sexo</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{route('admin.estudios')}}" class="nav_link nav_link--inside">Estudios</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{route('admin.parentesco')}}" class="nav_link nav_link--inside">Parentesco</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.pais') }}" class="nav_link nav_link--inside">Pais</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.departamento') }}" class="nav_link nav_link--inside">Departamento</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{route('admin.provincia')}}" class="nav_link nav_link--inside">Provincia</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{route('admin.distrito')}}" class="nav_link nav_link--inside">Distrito</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.tipoVia') }}" class="nav_link nav_link--inside">Tipo de vía</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.tipo') }}" class="nav_link nav_link--inside">Tipo</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.tipoZona') }}" class="nav_link nav_link--inside">Tipo de zona</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.condicion') }}" class="nav_link nav_link--inside">Condicion (C/N)</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.categoria') }}" class="nav_link nav_link--inside">Categoria laboral</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.dedicacionLaboral') }}" class="nav_link nav_link--inside">Dedicación
                        laboral</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{ route('admin.regimenPensionario') }}" class="nav_link nav_link--inside">Régimen
                        pensionario</a>
                </li>
                <li class="list_inside">
                    <i class="fa-solid fa-user-tie list_img"></i>
                    <a href="{{route('admin.formacionProfesional')}}" class="nav_link nav_link--inside">Formación profesional</a>
                </li>
            </ul>
        </li>

        {{-- <li class="list_item">
            <div class="list_button">
                <i class="fa-solid fa-briefcase list_img"></i>
                <a href="#" class="nav_link">Virtual</a>
            </div>
        </li> --}}
    </ul>
</aside>
