<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Género de los usuarios</legend>
            <div class="flex justify-end">
                <x-button wire:click="confirmSexoAdd" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo género') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Género</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sexos as $item)
                            <tr>
                                <td>{{ $contador }}</td>
                                <td>{{ $item->sexo }}</td>
                                <td>
                                    <x-secondary-button wire:click="confirmSexoEdit ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="confirmSexoDeletion ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="confirmingSexoDeletion">
                <x-slot name="title">
                    {{ __('Eliminar género') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el Género?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingSexoDeletion', false)"
                        wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="deleteSexo ({{ $confirmingSexoDeletion }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="confirmingSexoAdd">
                <x-slot name="title">
                    {{ isset($this->sexoModel->id) ? 'Editar género' : 'Crear género' }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="sexoModel.sexo" value="{{ __('Género') }}" />
                        <x-input id="sexoModel.sexo" type="text" class="mt-1 block w-full" wire:model="sexo" required />
                        <x-input-error for="sexo" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingSexoAdd', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="saveSexo" wire:loading.attr="disabled">
                        {{ isset($this->sexoModel->id) ? 'Actualizar' : 'Guardar' }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
