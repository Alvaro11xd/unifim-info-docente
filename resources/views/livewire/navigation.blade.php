<!-- BARRA DE NAVEGACION -->
<nav id="nav" class="nav">
    @auth
        <div class="nav_menu">
            <i class="fa-solid fa-bars" id="btn_open"></i>
            <span class="texto_mayuscula font_mediana">{{ Auth()->user()->nombres }} {{Auth()->user()->apePaterno}}</span>
        </div>
        <!-- MENU VERTICAL -->
        <a href="#menu_vertical" class="nav_menu nav_menu_vertical">
            <i class="fa-solid fa-ellipsis-vertical"></i>
        </a>
        <ul class="dropdown" id="menu_vertical">
            <small>Roles</small>
            <div class="dropdown_perfil">
                <img src="{{ Auth()->user()->profile_photo_url }}" alt="foto_usuario">
                <h5 class="texto_mayuscula">{{ Auth()->user()->nombres }}</h5>
            </div>
            <hr>
            <li class="dropdown_list">
                <a href="{{ route('profile.show') }}" class="dropdown_link">
                    <span class="dropdown_span">Mi información</span>
                </a>
            </li>
            <li class="dropdown_list">
                <a href="#" class="dropdown_link">
                    <span class="dropdown_span">Reiniciar clave</span>
                </a>
            </li>
            <li class="dropdown_list">
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}" x-data>
                    @csrf
                    <a href="{{ route('logout') }}" @click.prevent="$root.submit();" class="dropdown_link"
                        id="botonCerrarSesion">
                        <span class="dropdown_span">Cerrar sesión</span>
                    </a>
                </form>
            </li>
        </ul>
    @else
        <div>
            <a href="{{ route('login') }}" class="boton">Login</a>
            <a href="{{ route('register') }}" class="boton">Register</a>
        </div>
    @endauth
</nav>
