<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Provincias</legend>
            <div class="flex justify-end">
                <x-button wire:click="crear" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nueva provincia') }}
                </x-button>
            </div>
            <!-- TABLA -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">#</th>
                            <th class="texto_capitalize">Provincia</th>
                            <th class="texto_capitalize">Departamento</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($provincias as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->provincia }}</td>
                                <td>{{ $item->departamento->departamento }}</td>
                                <td>
                                    <x-secondary-button wire:click="editar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="modalEliminar ({{ $item->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $provincias->links() }}
                </div>
            </div>

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="idEliminar">
                <x-slot name="title">
                    {{ __('Eliminar provincia') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar la Provincia?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('idEliminar', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="eliminar ({{ $idEliminar }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="modalCrear">
                <x-slot name="title">
                    {{ $modalTitulo }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="provinciaModel.provincia" value="{{ __('Provincia') }}" />
                        <x-input id="provinciaModel.provincia" type="text" class="mt-1 block w-full"
                            wire:model="provincia" required />
                        <x-input-error for="provincia" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <label class="block text-sm font-medium text-gray-700">
                            Selecciona un departamento
                        </label>
                        <select wire:model.live="idDepartamento"
                            class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option>Seleccionar</option>
                            @foreach ($departamentos as $departamento)
                                <option value="{{ $departamento->id }}"
                                    {{ $idDepartamento == $departamento->id ? 'selected' : '' }}>
                                    {{ $departamento->departamento }}</option>
                            @endforeach
                        </select>
                        <x-input-error for="idDepartamento" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('modalCrear', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="guardar" wire:loading.attr="disabled">
                        {{ $botonTitulo }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
