<div class="contenedor_formulario my-4">
    <div class="caja_contenido">
        <div class="contenido d-block">
            <legend class="contenido_titulo texto_mayuscula">Estados civil</legend>
            <div class="flex justify-end">
                <x-button wire:click="confirmEstadoAdd" class="bg-blue-700 hover:bg-blue-900">
                    {{ __('Crear nuevo estado') }}
                </x-button>
            </div>
            <!-- TABLA DOMICILIO -->
            <div class="tabla_contenedor">
                <table class="tabla" border="1">
                    <thead>
                        <tr>
                            <th class="texto_capitalize">Id</th>
                            <th class="texto_capitalize">Estado</th>
                            <th class="texto_capitalize">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($estados as $estado)
                            <tr>
                                <td>{{ $contador }}</td>
                                <td>{{ $estado->civil }}</td>
                                <td>
                                    <x-secondary-button wire:click="confirmEstadoEdit ({{ $estado->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Editar') }}
                                    </x-secondary-button>
                                    <x-danger-button wire:click="confirmEstadoDeletion ({{ $estado->id }})"
                                        wire:loading.attr="disabled">
                                        {{ __('Eliminar') }}
                                    </x-danger-button>
                                </td>
                            </tr>
                            @php
                                $contador++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{-- <div class="mt-4">
                {{ $estados->links() }}
            </div> --}}

            <!-- Modal de confirmacion para eliminar un estado civil -->
            <x-dialog-modal wire:model.live="confirmingEstadoDeletion">
                <x-slot name="title">
                    {{ __('Eliminar estado') }}
                </x-slot>

                <x-slot name="content">
                    {{ __('¿Está seguro que desea eliminar el Estado?') }}
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingEstadoDeletion', false)"
                        wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="deleteEstado ({{ $confirmingEstadoDeletion }})"
                        wire:loading.attr="disabled">
                        {{ __('Eliminar') }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>

            <!-- Modal para crear un estado civil -->
            <x-dialog-modal wire:model.live="confirmingEstadoAdd">
                <x-slot name="title">
                    {{ isset($this->estadoCivil->id) ? 'Editar estado' : 'Crear estado' }}
                </x-slot>

                <x-slot name="content">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-4 mb-4">
                        <x-label for="estadoCivil.civil" value="{{ __('Estado civil') }}" />
                        <x-input id="estadoCivil.civil" type="text" class="mt-1 block w-full" wire:model="civil"
                            required />
                        <x-input-error for="civil" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="footer">
                    <x-secondary-button wire:click="$toggle('confirmingEstadoAdd', false)" wire:loading.attr="disabled">
                        {{ __('Cancelar') }}
                    </x-secondary-button>

                    <x-danger-button class="ms-3" wire:click="saveEstado" wire:loading.attr="disabled">
                        {{ isset($this->estadoCivil->id) ? 'Actualizar' : 'Guardar' }}
                    </x-danger-button>
                </x-slot>
            </x-dialog-modal>
        </div>
    </div>
</div>
