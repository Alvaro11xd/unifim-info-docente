<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- FAVICON -->
    <link rel="shortcut icon" href="{{ asset('assets/img/logo_uni_favicon.png') }}" type="image/x-icon">
    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/a9045ee35a.js" crossorigin="anonymous"></script>

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"
        integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>

    <!-- FUENTES DE GOOGLE -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&family=Rubik:wght@300;400;500;700&display=swap"
        rel="stylesheet">

    <!-- SWEET ALERT 2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>

    <title>Información Docente</title>

    <!-- Scripts -->
    @if (request()->routeIs([
            'profile.show',
            'admin.estadoCivil',
            'admin.sexo',
            'admin.pais',
            'admin.departamento',
            'admin.condicion',
            'admin.categoria',
            'admin.estudios',
            'admin.parentesco',
            'admin.tipoVia',
            'admin.tipo',
            'admin.tipoZona',
            'admin.dedicacionLaboral',
            'admin.regimenPensionario',
            'admin.formacionProfesional',
            'admin.provincia',
            'admin.distrito',
            'admin.usuarios'
        ]))
        <!-- ESTILOS CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    @else
        <!-- ESTILOS CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @endif

    <!-- Styles -->
    @livewireStyles
</head>

<body id="body" class="body_move">
    {{-- <x-banner /> --}}

    <div class="min-h-screen bg-gray-100">

        @livewire('navigation')
        <div class="overlay" id="overlay"></div>
        @livewire('sidebar')

        <!-- Page Content -->
        <main id="main">
            {{ $slot }}
        </main>
    </div>

    @stack('modals')

    <!-- SCRIPT JS -->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/infoDocente.js') }}"></script>
    @livewireScripts
</body>

</html>
