<?php

namespace App\Http\Controllers\Docente;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDomicilioRequest;
use App\Models\Domicilio;
use Illuminate\Http\Request;

class DomicilioController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(StoreDomicilioRequest $request)
    {
        $data = $request->all();

        // Asegúrate de que los valores estén presentes
        if (isset($data['pais'], $data['provincia'], $data['departamento'], $data['distrito'])) {
            // Crea el Domicilio con los valores necesarios
            Domicilio::create([
                'user_id' => auth()->user()->id,
                'idVia' => $data['tipo_via'],
                'idTipo' => $data['tipo'],
                'nombreVia' => $data['nombre_via'],
                'numeroInmueble' => $data['numero_inmueble'],
                'idZona' => $data['tipo_zona'],
                'nombreZona' => $data['nombre_zona'],
                'idPais' => $data['pais'],
                'idProvincia' => $data['provincia'],
                'idDepartamento' => $data['departamento'],
                'idDistrito' => $data['distrito'],
            ]);
        }

        return redirect()->route('contenido.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Domicilio $domicilio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Domicilio $domicilio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreDomicilioRequest $request, Domicilio $domicilio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Domicilio $domicilio)
    {
        //
    }
}
