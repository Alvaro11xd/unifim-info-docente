<?php

namespace App\Http\Controllers;

use App\Models\DatosPersonales;
use Illuminate\Http\Request;

class DatosPersonalesController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $request->validate([
            'sexo' => ['required'],
            'fecha_nacimiento' => ['required', 'date'],
            'telefono' => ['required'],
            'ruc' => ['nullable', 'string'],
            'brevete' => ['nullable', 'string'],
            'estado_civil' => ['nullable', 'string'],
            'fecha_matrimonio' => ['nullable', 'date'],
            'dni' => ['nullable', 'string'],
            'celular' => ['nullable', 'string'],
            'lib_militar' => ['nullable', 'string'],
            'vehiculo' => ['nullable', 'string'],
            'grupo_sanguineo' => ['nullable', 'string'],
        ]);

        $nuevoRegistro = new DatosPersonales();

        $nuevoRegistro->sexo = $request->input('sexo');
        $nuevoRegistro->sexo = $request->input('fecha_nacimiento');
        $nuevoRegistro->sexo = $request->input('telefono');
        $nuevoRegistro->sexo = $request->input('ruc');
        $nuevoRegistro->sexo = $request->input('brevete');
        $nuevoRegistro->sexo = $request->input('estado_civil');
        $nuevoRegistro->sexo = $request->input('fecha_matrimonio');
        $nuevoRegistro->sexo = $request->input('dni');
        $nuevoRegistro->sexo = $request->input('celular');
        $nuevoRegistro->sexo = $request->input('lib_militar');
        $nuevoRegistro->sexo = $request->input('vehiculo');
        $nuevoRegistro->sexo = $request->input('grupo_sanguineo');
        $nuevoRegistro->user_id = $request->user()->id;

        $nuevoRegistro->save();

        return redirect()->route('contenido.index')->with('success', 'Registro creado exitosamente');
    }
    public function show(string $id)
    {
        //
    }
    public function edit(string $id)
    {
        //
    }
    public function update(Request $request, string $id)
    {
        //
    }
    public function destroy(string $id)
    {
        //
    }
}
