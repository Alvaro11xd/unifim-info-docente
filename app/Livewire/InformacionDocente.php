<?php

namespace App\Livewire;

use Livewire\Component;

class InformacionDocente extends Component
{
    public function render()
    {
        return view('livewire.informacion-docente');
    }
}
