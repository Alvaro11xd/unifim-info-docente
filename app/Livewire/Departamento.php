<?php

namespace App\Livewire;

use App\Models\Departamento as ModelsDepartamento;
use App\Models\Pais;
use Livewire\Component;
use Livewire\WithPagination;

class Departamento extends Component
{
    use WithPagination;
    public $departamentoModel;
    public $departamento;
    public $paisId; // Propiedad para almacenar el id del país seleccionado
    public $confirmingDepartamentoDeletion = false;
    public $confirmingDepartamentoAdd = false;
    protected $rules = [
        'departamento' => 'required',
        'paisId' => 'required|exists:pais,id', // Verificar que el país seleccionado exista en la tabla pais
    ];
    public function render()
    {
        $departamentos = ModelsDepartamento::paginate(10);
        $paises = Pais::all(); // Obtener todos los países para el dropdown
        $contador = 1;
        
        return view('livewire.departamento', compact('departamentos', 'contador', 'paises'));
    }
    public function confirmDepartamentoDeletion($id)
    {
        $this->confirmingDepartamentoDeletion = $id;
    }

    public function deleteDepartamento(ModelsDepartamento $DepartamentoModel)
    {
        $DepartamentoModel->delete();
        $this->confirmingDepartamentoDeletion = false;
    }
    public function confirmDepartamentoAdd()
    {
        $this->reset(['departamento', 'paisId', 'departamentoModel']);
        $this->confirmingDepartamentoAdd = true;
    }

    public function saveDepartamento()
    {
        $this->validate();

        if ($this->departamentoModel && $this->departamentoModel->exists) {
            $this->departamentoModel->update([
                'departamento' => $this->departamento,
                'idPais' => $this->paisId, // Asignar el id del país al departamento
            ]);
        } else {
            ModelsDepartamento::create([
                'departamento' => $this->departamento,
                'idPais' => $this->paisId, // Asignar el id del país al nuevo departamento
            ]);
        }
        $this->confirmingDepartamentoAdd = false;
        $this->reset(['departamento', 'paisId', 'departamentoModel']);
    }

    public function confirmDepartamentoEdit(ModelsDepartamento $departamentoModel)
    {
        $this->departamentoModel = $departamentoModel;
        $this->departamento = $departamentoModel->departamento;
        $this->paisId = $departamentoModel->pais->id;
        $this->confirmingDepartamentoAdd = true;
    }
}
