<?php

namespace App\Livewire;

use App\Models\Sexo as ModelsSexo;
use Livewire\Component;
use Livewire\WithPagination;

class Sexo extends Component
{
    use WithPagination;
    public $sexoModel;  // Esta variable almacena una instancia del modelo ModelsSexo
    public $sexo;  // Esta variable almacena el valor del campo en el formulario
    public $confirmingSexoDeletion = false;
    public $confirmingSexoAdd = false;

    protected $rules = [
        'sexo' => 'required',
    ];
    public function render()
    {
        $sexos = ModelsSexo::all();
        $contador = 1;
        return view('livewire.sexo', compact('sexos', 'contador'));
    }
    public function confirmSexoDeletion($id)
    {
        $this->confirmingSexoDeletion = $id;
    }

    public function deleteSexo(ModelsSexo $sexoModel)
    {
        $sexoModel->delete();
        $this->confirmingSexoDeletion = false;
    }
    public function confirmSexoAdd()
    {
        $this->reset(['sexo', 'sexoModel']);
        $this->confirmingSexoAdd = true;
    }

    public function saveSexo()
    {
        $this->validate();

        if ($this->sexoModel && $this->sexoModel->exists) {
            $this->sexoModel->update([
                'sexo' => $this->sexo,
            ]);
        } else {
            ModelsSexo::create([
                'sexo' => $this->sexo
            ]);
        }
        $this->confirmingSexoAdd = false;
    }

    public function confirmSexoEdit(ModelsSexo $sexoModel)
    {
        $this->sexoModel = $sexoModel;
        $this->sexo = $sexoModel->sexo;
        $this->confirmingSexoAdd = true;
    }
}
