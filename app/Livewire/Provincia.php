<?php

namespace App\Livewire;

use App\Models\Departamento;
use App\Models\Provincia as ModelsProvincia;
use Livewire\Component;
use Livewire\WithPagination;

class Provincia extends Component
{
    use WithPagination;
    public $provinciaModel; // Almacena una instancia del modelo Provincia para su uso en operaciones de edición.
    public $provincia, $id_provincia; // Almacena el valor del campo "provincia" del formulario.
    public $idDepartamento; // Propiedad para almacenar el id del país seleccionado
    public $modalTitulo;
    public $botonTitulo;
    public $idEliminar = false;
    public $modalCrear = false;
    protected $rules = [
        'provincia' => 'required',
        'idDepartamento' => 'required|exists:departamentos,id', // Verificar que el país seleccionado exista en la tabla departamentos
    ];
    public function render()
    {
        $provincias = ModelsProvincia::paginate(10);
        $departamentos = Departamento::all(); // Obtener todos los departamentos para el dropdown
        return view('livewire.provincia', compact('provincias', 'departamentos'));
    }

    public function modalEliminar($id)
    {
        $this->idEliminar = $id;
    }

    public function eliminar($id)
    {
        ModelsProvincia::find($id)->delete();
        $this->idEliminar = false;
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear provincia';
        $this->botonTitulo = 'Guardar';
        $this->modalCrear = true;
    }

    public function limpiarCampos()
    {
        $this->provincia = '';
        $this->idDepartamento = '';
        $this->provinciaModel = '';
    }

    public function guardar()
    {
        $this->validate();

        ModelsProvincia::updateOrCreate(
            ['id' => $this->id_provincia],
            [
                'provincia' => $this->provincia,
                'idDepartamento' => $this->idDepartamento
            ]
        );
        session()->flash('message', $this->id_provincia ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->modalCrear = false;
        $this->limpiarCampos();
    }

    public function editar($id)
    {
        $provincia = ModelsProvincia::findOrFail($id);
        $this->id_provincia = $id;
        $this->provincia = $provincia->provincia;
        $this->idDepartamento = $provincia->departamento->id;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->modalCrear = true;
    }
}
