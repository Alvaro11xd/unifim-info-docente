<?php

namespace App\Livewire;

use App\Models\Pais as ModelsPais;
use Livewire\Component;
use Livewire\WithPagination;

class Pais extends Component
{
    use WithPagination;
    public $paisModel;
    public $pais;
    public $confirmingPaisDeletion = false;
    public $confirmingPaisAdd = false;
    protected $rules = [
        'pais' => 'required',
    ];
    public function render()
    {
        $paises = ModelsPais::all();
        $contador = 1;
        return view('livewire.pais', compact('paises', 'contador'));
    }
    public function confirmPaisDeletion($id)
    {
        $this->confirmingPaisDeletion = $id;
    }

    public function deletePais(ModelsPais $paisModel)
    {
        $paisModel->delete();
        $this->confirmingPaisDeletion = false;
    }
    public function confirmPaisAdd()
    {
        $this->reset(['pais', 'paisModel']);
        $this->confirmingPaisAdd = true;
    }

    public function savePais()
    {
        $this->validate();

        if ($this->paisModel && $this->paisModel->exists) {
            $this->paisModel->update([
                'pais' => $this->pais,
            ]);
        } else {
            ModelsPais::create([
                'pais' => $this->pais
            ]);
        }
        $this->confirmingPaisAdd = false;
    }

    public function confirmPaisEdit(ModelsPais $paisModel)
    {
        $this->paisModel = $paisModel;
        $this->pais = $paisModel->pais;
        $this->confirmingPaisAdd = true;
    }
}
