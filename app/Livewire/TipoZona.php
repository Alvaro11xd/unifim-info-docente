<?php

namespace App\Livewire;

use App\Models\TipoZona as ModelsTipoZona;
use Livewire\Component;
use Livewire\WithPagination;

class TipoZona extends Component
{
    use WithPagination;
    public $tiposZona, $zona, $id_zona;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'zona' => 'required',
    ];
    public function render()
    {
        $this->tiposZona = ModelsTipoZona::all();
        return view('livewire.tipo-zona');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->zona = '';
        $this->id_zona = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_zona = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $registro = ModelsTipozona::findOrFail($id);
        $this->id_zona = $id;
        $this->zona = $registro->zona;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsTipozona::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsTipozona::updateOrCreate(
            ['id' => $this->id_zona],
            [
                'zona' => $this->zona
            ]
        );
        session()->flash('message', $this->id_zona ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
