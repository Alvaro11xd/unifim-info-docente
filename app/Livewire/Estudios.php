<?php

namespace App\Livewire;

use App\Models\Estudios as ModelsEstudios;
use Livewire\Component;
use Livewire\WithPagination;

class Estudios extends Component
{
    use WithPagination;
    public $estudios, $estudio, $id_estudio;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'estudio' => 'required',
    ];
    public function render()
    {
        $this->estudios = ModelsEstudios::all();
        return view('livewire.estudios');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->estudio = '';
        $this->id_estudio = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_estudio = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $estudio = ModelsEstudios::findOrFail($id);
        $this->id_estudio = $id;
        $this->estudio = $estudio->estudios;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsEstudios::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsEstudios::updateOrCreate(
            ['id' => $this->id_estudio],
            [
                'estudios' => $this->estudio
            ]
        );
        session()->flash('message', $this->id_estudio ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
