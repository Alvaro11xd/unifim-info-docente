<?php

namespace App\Livewire;

use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\Pais;
use App\Models\Provincia;
use Livewire\Component;

class Dropdown extends Component
{
    public $selectedPais = null, $selectedDepartamento = null, $selectedProvincia = null, $selectedDistrito = null;
    public $departamentos = null, $provincias = null, $distritos = null;

    public function render()
    {
        $paises = Pais::all();
        return view('livewire.dropdown', compact('paises'));
    }
    public function updatedSelectedPais($idPais){
        $this->departamentos = Departamento::where('idPais', $idPais)->get();
        $this->dispatch('selectedPais', $idPais);
        $this->dispatch('selectedDepartamento', null); // Resetea el departamento seleccionado
        $this->dispatch('selectedProvincia', null); // Resetea la provincia seleccionada
        $this->dispatch('selectedDistrito', null); // Resetea el distrito seleccionado
    }
    public function updatedSelectedDepartamento($idDepartamento)
    {
        $this->provincias = Provincia::where('idDepartamento', $idDepartamento)->get();
    }
    public function updatedSelectedProvincia($idProvincia)
    {
        $this->distritos = Distrito::where('idProvincia', $idProvincia)->get();
    }

    
}
