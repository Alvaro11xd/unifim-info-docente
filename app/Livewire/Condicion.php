<?php

namespace App\Livewire;

use App\Models\Condicion as ModelsCondicion;
use Livewire\Component;
use Livewire\WithPagination;

class Condicion extends Component
{
    use WithPagination;
    public $condicionModel;  // Esta variable almacena una instancia del modelo Modelscondicion
    public $condicion;  // Esta variable almacena el valor del campo en el formulario
    public $confirmingCondicionDeletion = false;
    public $confirmingCondicionAdd = false;
    public function render()
    {
        $condiciones = ModelsCondicion::all();
        $contador = 1;
        return view('livewire.condicion', compact('condiciones', 'contador'));
    }
    protected $rules = [
        'condicion' => 'required',
    ];
    public function confirmCondicionDeletion($id)
    {
        $this->confirmingCondicionDeletion = $id;
    }

    public function deleteCondicion(ModelsCondicion $condicionModel)
    {
        $condicionModel->delete();
        $this->confirmingCondicionDeletion = false;
    }
    public function confirmCondicionAdd()
    {
        $this->reset(['condicion', 'condicionModel']);
        $this->confirmingCondicionAdd = true;
    }

    public function saveCondicion()
    {
        $this->validate();

        if ($this->condicionModel && $this->condicionModel->exists) {
            $this->condicionModel->update([
                'condicion' => $this->condicion,
            ]);
        } else {
            Modelscondicion::create([
                'condicion' => $this->condicion
            ]);
        }
        $this->confirmingCondicionAdd = false;
    }

    public function confirmCondicionEdit(Modelscondicion $condicionModel)
    {
        $this->condicionModel = $condicionModel;
        $this->condicion = $condicionModel->condicion;
        $this->confirmingCondicionAdd = true;
    }
}
