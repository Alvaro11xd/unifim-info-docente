<?php

namespace App\Livewire;

use Livewire\Component;

class Bienvenida extends Component
{
    public function render()
    {
        return view('livewire.bienvenida');
    }
}
