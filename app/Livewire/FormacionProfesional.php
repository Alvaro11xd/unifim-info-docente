<?php

namespace App\Livewire;

use App\Models\FormacionProfesional as ModelsFormacionProfesional;
use Livewire\Component;
use Livewire\WithPagination;

class FormacionProfesional extends Component
{
    use WithPagination;
    public $formaciones, $formacion, $id_formacion;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'formacion' => 'required',
    ];
    public function render()
    {
        $this->formaciones = ModelsFormacionProfesional::all();
        return view('livewire.formacion-profesional');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->formacion = '';
        $this->id_formacion = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_formacion = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $formacion = ModelsFormacionProfesional::findOrFail($id);
        $this->id_formacion = $id;
        $this->formacion = $formacion->formacion;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsFormacionProfesional::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsFormacionProfesional::updateOrCreate(
            ['id' => $this->id_formacion],
            [
                'formacion' => $this->formacion
            ]
        );
        session()->flash('message', $this->id_formacion ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
