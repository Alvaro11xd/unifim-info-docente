<?php

namespace App\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithPagination;

class Usuario extends Component
{
    use WithPagination;
    public $usuarios, $apePaterno,$apeMaterno,$nombres,$fecNacimiento,$estado,$usuario,$email,$password, $id_usuario;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;
    protected $rules = [
        'usuario' => 'required',
    ];
    public function render()
    {
        $this->usuarios = User::all();
        return view('livewire.usuario');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->apePaterno = '';
        $this->apeMaterno = '';
        $this->nombres = '';
        $this->fecNacimiento = '';
        $this->estado = '';
        $this->usuario = '';
        $this->email = '';
        $this->password = '';
        $this->id_usuario = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_usuario = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $usuario = User::findOrFail($id);
        $this->id_usuario = $id;
        $this->apePaterno = $usuario->apePaterno;
        $this->apeMaterno = $usuario->apeMaterno;
        $this->nombres = $usuario->nombres;
        $this->fecNacimiento = $usuario->fecNacimiento;
        $this->estado = $usuario->estado;
        $this->usuario = $usuario->usuario;
        $this->email = $usuario->email;
        $this->password = $usuario->password;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        User::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        User::updateOrCreate(
            ['id' => $this->id_usuario],
            [
                'apePaterno' => $this->apePaterno,
                'apeMaterno' => $this->apeMaterno,
                'nombres' => $this->nombres,
                'fecNacimiento' => $this->fecNacimiento,
                'usuario' => $this->usuario,
                'email' => $this->email,
                'password' => Hash::make($this->password),
            ]
        );
        session()->flash('message', $this->id_usuario ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
