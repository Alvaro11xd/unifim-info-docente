<?php

namespace App\Livewire;

use App\Models\Distrito as ModelsDistrito;
use App\Models\Provincia;
use Livewire\Component;
use Livewire\WithPagination;

class Distrito extends Component
{
    use WithPagination;
    public $distritoModel;
    public $distrito, $id_distrito;
    public $idProvincia;
    public $modalTitulo;
    public $botonTitulo;
    public $idEliminar = false;
    public $modalCrear = false;
    protected $rules = [
        'distrito' => 'required',
        'idProvincia' => 'required|exists:provincias,id',
    ];
    public function render()
    {
        $distritos = ModelsDistrito::paginate(10);
        $provincias = Provincia::all();
        return view('livewire.distrito', compact('distritos', 'provincias'));
    }
    public function modalEliminar($id)
    {
        $this->idEliminar = $id;
    }

    public function eliminar($id)
    {
        ModelsDistrito::find($id)->delete();
        $this->idEliminar = false;
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear distrito';
        $this->botonTitulo = 'Guardar';
        $this->modalCrear = true;
    }

    public function limpiarCampos()
    {
        $this->distrito = '';
        $this->idProvincia = '';
        $this->distritoModel = '';
    }

    public function guardar()
    {
        $this->validate();

        ModelsDistrito::updateOrCreate(
            ['id' => $this->id_distrito],
            [
                'distrito' => $this->distrito,
                'idProvincia' => $this->idProvincia
            ]
        );
        session()->flash('message', $this->id_distrito ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->modalCrear = false;
        $this->limpiarCampos();
    }

    public function editar($id)
    {
        $distrito = ModelsDistrito::findOrFail($id);
        $this->id_distrito = $id;
        $this->distrito = $distrito->distrito;
        $this->idProvincia = $distrito->provincia->id;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->modalCrear = true;
    }
}
