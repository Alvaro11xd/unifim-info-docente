<?php

namespace App\Livewire\Docente;

use App\Models\Domicilio;
use Livewire\Component;

use Livewire\WithPagination;

use App\Models\CategoriaLaboral;
use App\Models\Condicion;
use App\Models\DedicacionLaboral;
use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\EstadoCivil;
use App\Models\Estudios;
use App\Models\FormacionProfesional;
use App\Models\Pais;
use App\Models\Parentesco;
use App\Models\Provincia;
use App\Models\RegimenPensionario;
use App\Models\Sexo;
use App\Models\TipoVia;
use App\Models\Tipo;
use App\Models\TipoZona;

class Index extends Component
{
    use WithPagination;

    public $selectedPais = null, $selectedDepartamento = null, $selectedProvincia = null, $selectedDistrito = null;
    public $departamentos = null, $provincias = null, $distritos = null;

    public $tipo_via, $tipo, $tipo_zona, $nombre_via, $numero_inmueble, $nombre_zona;

    public $modalTitulo;
    public $botonTitulo;
    public $modalCrear = false;

    protected $rules = [
        'tipo_via' => 'required',
        'tipo' => 'required',
        'tipo_zona' => 'required',
        'nombre_via' => 'required',
        'numero_inmueble' => 'required',
        'nombre_zona' => 'required',
    ];

    public function render()
    {
        $estados = EstadoCivil::all();
        $sexos = Sexo::all();
        $condiciones = Condicion::all();
        $categorias = CategoriaLaboral::all();
        $estudios = Estudios::all();
        $parentescos = Parentesco::all();
        $tiposVia = TipoVia::all();
        $tipos = Tipo::all();
        $tiposZona = TipoZona::all();
        $dedicaciones = DedicacionLaboral::all();
        $regimenes = RegimenPensionario::all();
        $formaciones = FormacionProfesional::all();
        $paises = Pais::all();

        // Domicilios del usuario autentificado
        $domicilios = Domicilio::where('idUser', auth()->user()->id)->paginate(5);
        return view('livewire.docente.index', compact("estados", "sexos", "condiciones", "categorias", "estudios", "parentescos", "tiposVia", "tipos", "tiposZona", "dedicaciones", "regimenes", "formaciones", "paises", "domicilios"));
    }
    public function updatedSelectedPais($idPais)
    {
        $this->departamentos = Departamento::where('idPais', $idPais)->get();
    }
    public function updatedSelectedDepartamento($idDepartamento)
    {
        $this->provincias = Provincia::where('idDepartamento', $idDepartamento)->get();
    }
    public function updatedSelectedProvincia($idProvincia)
    {
        $this->distritos = Distrito::where('idProvincia', $idProvincia)->get();
    }

    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'agregar domicilio';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->tipo_via = '';
        $this->tipo_zona = '';
        $this->tipo_via = '';
        $this->nombre_via = '';
        $this->numero_inmueble = '';
        $this->nombre_zona = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
    }


    public function guardar()
    {
        $this->validate();
        Domicilio::updateOrCreate(
            [
                'idPais' => $this->selectedPais,
                'idVia' => $this->tipo_via,
                'nombreVia' => $this->nombre_via,
                'idTipo' => $this->tipo,
                'numeroInmueble' => $this->numero_inmueble,
                'idZona' => $this->tipo_zona,
                'nombreZona' => $this->nombre_zona,
                'idUser' => auth()->user()->id
            ]
        );
        session()->flash('message', 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
