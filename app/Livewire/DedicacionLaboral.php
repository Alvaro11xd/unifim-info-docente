<?php

namespace App\Livewire;

use App\Models\DedicacionLaboral as ModelsDedicacionLaboral;
use Livewire\Component;
use Livewire\WithPagination;

class DedicacionLaboral extends Component
{
    use WithPagination;
    public $dedicaciones, $dedicacion, $id_dedicacion;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'dedicacion' => 'required',
    ];
    public function render()
    {
        $this->dedicaciones = ModelsDedicacionLaboral::all();
        return view('livewire.dedicacion-laboral');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->dedicacion = '';
        $this->id_dedicacion = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_dedicacion = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $registro = ModelsDedicacionLaboral::findOrFail($id);
        $this->id_dedicacion = $id;
        $this->dedicacion = $registro->dedicacion;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsDedicacionLaboral::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsDedicacionLaboral::updateOrCreate(
            ['id' => $this->id_dedicacion],
            [
                'dedicacion' => $this->dedicacion
            ]
        );
        session()->flash('message', $this->id_dedicacion ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
