<?php

namespace App\Livewire;

use App\Models\CategoriaLaboral;
use Livewire\Component;
use Livewire\WithPagination;


class Categoria extends Component
{
    use WithPagination;
    public $categoriaModel;  // Esta variable almacena una instancia del modelo Modelscategoria
    public $categoria;  // Esta variable almacena el valor del campo en el formulario
    public $confirmingCategoriaDeletion = false;
    public $confirmingCategoriaAdd = false;

    protected $rules = [
        'categoria' => 'required',
    ];
    public function render()
    {
        $categorias = CategoriaLaboral::all();
        $contador = 1;
        return view('livewire.categoria', compact('categorias', 'contador'));
    }
    public function confirmCategoriaDeletion($id)
    {
        $this->confirmingCategoriaDeletion = $id;
    }

    public function deleteCategoria(CategoriaLaboral $categoriaModel)
    {
        $categoriaModel->delete();
        $this->confirmingCategoriaDeletion = false;
    }
    public function confirmCategoriaAdd()
    {
        $this->reset(['categoria', 'categoriaModel']);
        $this->confirmingCategoriaAdd = true;
    }

    public function saveCategoria()
    {
        $this->validate();

        if ($this->categoriaModel && $this->categoriaModel->exists) {
            $this->categoriaModel->update([
                'categoria' => $this->categoria,
            ]);
        } else {
            CategoriaLaboral::create([
                'categoria' => $this->categoria
            ]);
        }
        $this->confirmingCategoriaAdd = false;
    }

    public function confirmCategoriaEdit(CategoriaLaboral $categoriaModel)
    {
        $this->categoriaModel = $categoriaModel;
        $this->categoria = $categoriaModel->categoria;
        $this->confirmingCategoriaAdd = true;
    }
}
