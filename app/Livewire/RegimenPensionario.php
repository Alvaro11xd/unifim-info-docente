<?php

namespace App\Livewire;

use App\Models\RegimenPensionario as ModelsRegimenPensionario;
use Livewire\Component;
use Livewire\WithPagination;

class RegimenPensionario extends Component
{
    use WithPagination;
    public $regimenes, $regimen, $id_regimen;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'regimen' => 'required',
    ];
    public function render()
    {
        $this->regimenes = ModelsRegimenPensionario::all();
        return view('livewire.regimen-pensionario');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->regimen = '';
        $this->id_regimen = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_regimen = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $regimen = ModelsRegimenPensionario::findOrFail($id);
        $this->id_regimen = $id;
        $this->regimen = $regimen->regimen;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsRegimenPensionario::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsRegimenPensionario::updateOrCreate(
            ['id' => $this->id_regimen],
            [
                'regimen' => $this->regimen
            ]
        );
        session()->flash('message', $this->id_regimen ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
