<?php

namespace App\Livewire;

use App\Models\TipoVia as ModelsTipoVia;
use Livewire\Component;
use Livewire\WithPagination;

class TipoVia extends Component
{
    use WithPagination;
    public $tiposVia, $via, $id_via;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'via' => 'required',
    ];
    public function render()
    {
        $this->tiposVia = ModelsTipoVia::all();
        return view('livewire.tipo-via');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->via = '';
        $this->id_via = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_via = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $tipoVia = ModelsTipoVia::findOrFail($id);
        $this->id_via = $id;
        $this->via = $tipoVia->via;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsTipoVia::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsTipoVia::updateOrCreate(
            ['id' => $this->id_via],
            [
                'via' => $this->via
            ]
        );
        session()->flash('message', $this->id_via ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
