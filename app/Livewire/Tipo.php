<?php

namespace App\Livewire;

use App\Models\Tipo as ModelsTipo;
use Livewire\Component;
use Livewire\WithPagination;

class Tipo extends Component
{
    use WithPagination;
    public $tipos, $tipo, $id_tipo;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;
    protected $rules = [
        'tipo' => 'required',
    ];
    public function render()
    {
        $this->tipos = ModelsTipo::all();
        return view('livewire.tipo');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->tipo = '';
        $this->id_tipo = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_tipo = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $registro = ModelsTipo::findOrFail($id);
        $this->id_tipo = $id;
        $this->tipo = $registro->tipo;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsTipo::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsTipo::updateOrCreate(
            ['id' => $this->id_tipo],
            [
                'tipo' => $this->tipo
            ]
        );
        session()->flash('message', $this->id_tipo ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
