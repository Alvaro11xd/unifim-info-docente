<?php

namespace App\Livewire;

use App\Models\Parentesco as ModelsParentesco;
use Livewire\Component;
use Livewire\WithPagination;

class Parentesco extends Component
{
    use WithPagination;
    public $parentescos, $parentesco, $id_parentesco;
    public $modalTitulo;
    public $botonTitulo;
    public $modalEliminar = false;
    public $modalCrear = false;

    protected $rules = [
        'parentesco' => 'required',
    ];
    public function render()
    {
        $this->parentescos = ModelsParentesco::all();
        return view('livewire.parentesco');
    }
    public function crear()
    {
        $this->limpiarCampos();
        $this->modalTitulo = 'Crear registro';
        $this->botonTitulo = 'Guardar';
        $this->abrirModal();
    }

    public function limpiarCampos()
    {
        $this->parentesco = '';
        $this->id_parentesco = '';
    }
    public function abrirModal()
    {
        $this->modalCrear = true;
    }
    public function cerrarModal()
    {
        $this->modalCrear = false;
        $this->modalEliminar = false;
    }
    public function abrirModalEliminar($id)
    {
        $this->id_parentesco = $id;
        $this->modalEliminar = true;
    }

    public function cerrarModalEliminar()
    {
        $this->modalEliminar = false;
    }

    public function editar($id)
    {
        $parentesco = ModelsParentesco::findOrFail($id);
        $this->id_parentesco = $id;
        $this->parentesco = $parentesco->parentesco;
        $this->modalTitulo = 'Editar registro';
        $this->botonTitulo = 'Actualizar';
        $this->abrirModal();
    }

    public function eliminar($id)
    {
        ModelsParentesco::find($id)->delete();
        $this->cerrarModal();
    }


    public function guardar()
    {
        $this->validate();
        ModelsParentesco::updateOrCreate(
            ['id' => $this->id_parentesco],
            [
                'parentesco' => $this->parentesco
            ]
        );
        session()->flash('message', $this->id_parentesco ? '¡Actualización exitosa!' : 'Alta exitosa');
        $this->cerrarModal();
        $this->limpiarCampos();
    }
}
