<?php

namespace App\Livewire;

use App\Models\EstadoCivil as ModelsEstadoCivil;
use Livewire\Component;
use Livewire\WithPagination;

class EstadoCivil extends Component
{
    use WithPagination;
    public $estadoCivil;
    public $civil;
    public $confirmingEstadoDeletion = false;
    public $confirmingEstadoAdd = false;

    protected $rules = [
        'civil' => 'required',
    ];

    public function render()
    {
        $estados = ModelsEstadoCivil::paginate(5);
        $contador = 1;
        return view('livewire.estado-civil', compact(['estados', 'contador']));
    }
    public function confirmEstadoDeletion($id)
    {
        $this->confirmingEstadoDeletion = $id;
    }

    public function deleteEstado(ModelsEstadoCivil $estadoCivil)
    {
        $estadoCivil->delete();
        $this->confirmingEstadoDeletion = false;
    }
    public function confirmEstadoAdd()
    {
        $this->reset(['civil', 'estadoCivil']);
        $this->confirmingEstadoAdd = true;
    }

    public function saveEstado()
    {
        $this->validate();

        if ($this->estadoCivil && $this->estadoCivil->id) {
            $this->estadoCivil->update([
                'civil' => $this->civil,
            ]);
        } else {
            ModelsEstadoCivil::create([
                'civil' => $this->civil
            ]);
        }
        $this->confirmingEstadoAdd = false;
    }

    public function confirmEstadoEdit(ModelsEstadoCivil $estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
        $this->civil = $estadoCivil->civil;
        $this->confirmingEstadoAdd = true;
    }
}
