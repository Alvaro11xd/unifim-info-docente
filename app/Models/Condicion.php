<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Condicion extends Model
{
    use HasFactory;
    protected $fillable = ['condicion'];

    public function laboral()
    {
        return $this->hasOne(Laboral::class);
    }
}
