<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    use HasFactory;
    protected $fillable = ['departamento', 'idPais'];

    // Relacion de uno a muchos
    public function provincia()
    {
        return $this->hasMany(Provincia::class,'idDepartamento');
    }

    // Relacion de uno a muchos inversa
    public function pais()
    {
        return $this->belongsTo(Pais::class, 'idPais');
    }
}
