<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DedicacionLaboral extends Model
{
    use HasFactory;
    protected $fillable = ['dedicacion'];

    public function laboral(){
        return $this->hasOne(Laboral::class);
    }
}
