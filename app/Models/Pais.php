<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;
    protected $fillable = ['pais'];

    public function profesional()
    {
        return $this->hasOne(Profesional::class);
    }

    // Relacion de uno a muchos
    public function departamento()
    {
        return $this->hasMany(Departamento::class, 'idPais');
    }

    public function domicilio()
    {
        return $this->hasMany(Domicilio::class);
    }
}
