<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function pais()
    {
        return $this->belongsTo(Pais::class, 'idPais');
    }

    public function tipo()
    {
        return $this->belongsTo(Tipo::class, 'idTipo');
    }
    public function tipovia()
    {
        return $this->belongsTo(TipoVia::class, 'idVia');
    }
    public function tipozona()
    {
        return $this->belongsTo(TipoZona::class, 'idZona');
    }

    // Relacion uno a muchos inversa
    public function user(){
        $this->belongsTo(User::class);
    }
}
