<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laboral extends Model
{
    use HasFactory;

    public function condicion()
    {
        return $this->belongsTo(Condicion::class);
    }
    public function categoriaLaboral()
    {
        return $this->belongsTo(CategoriaLaboral::class);
    }
    public function dedicacionLaboral()
    {
        return $this->belongsTo(DedicacionLaboral::class);
    }
    public function regimenPensionario()
    {
        return $this->belongsTo(RegimenPensionario::class);
    }
}
