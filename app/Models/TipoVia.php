<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoVia extends Model
{
    use HasFactory;
    protected $fillable = ['via'];

    public function domicilio(){
        return $this->hasOne(Domicilio::class, 'idVia');
    }
}
