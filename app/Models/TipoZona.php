<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoZona extends Model
{
    use HasFactory;
    protected $fillable = ['zona'];

    public function domicilio(){
        return $this->hasOne(Domicilio::class);
    }
}
