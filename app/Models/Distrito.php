<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    use HasFactory;
    protected $fillable = ['distrito', 'idProvincia'];

    // Relacion de uno a muchos inversa
    public function provincia()
    {
        return $this->belongsTo(Provincia::class, 'idProvincia');
    }
}
