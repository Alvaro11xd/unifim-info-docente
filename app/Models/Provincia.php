<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    use HasFactory;
    protected $fillable = ['provincia', 'idDepartamento'];


    // Relacion de uno a muchos
    public function distrito()
    {
        return $this->hasMany(Distrito::class,'idProvincia');
    }

    // Relacion de uno a muchos inversa
    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'idDepartamento');
    }
}
