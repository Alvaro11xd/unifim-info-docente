<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegimenPensionario extends Model
{
    use HasFactory;
    protected $fillable = ['regimen'];

    // Relacion uno a uno
    public function laboral(){
        return $this->hasOne(Laboral::class);
    }
}
