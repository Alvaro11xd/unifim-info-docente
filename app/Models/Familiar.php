<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Familiar extends Model
{
    use HasFactory;

    // Relacion uno a muchos inversa

    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }
    public function estadocivil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }
    public function estudios()
    {
        return $this->belongsTo(Estudios::class);
    }
    public function parentesco()
    {
        return $this->belongsTo(Parentesco::class);
    }
    
}
