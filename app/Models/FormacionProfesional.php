<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormacionProfesional extends Model
{
    use HasFactory;
    protected $fillable = ['formacion'];

    public function profesional()
    {
        return $this->hasOne(Profesional::class);
    }
}
