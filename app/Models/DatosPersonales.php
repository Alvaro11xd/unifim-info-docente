<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatosPersonales extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    // Relacion uno a uno polimorfica

    public function image()
    {
        return $this->morphOne(FotoDocente::class, "imageable");
    }

    // Relacion uno a uno inversa

    public function sexo()
    {
        return $this->belongsTo(Sexo::class);
    }
    public function estadocivil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }
    // Relacion uno a muchos inversa
    public function user(){
        return $this->belongsTo(User::class);
    }
}
