<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'apePaterno'=> ['required', 'string', 'max:255'],
            'apeMaterno'=> ['required', 'string', 'max:255'],
            'nombres'=> ['required', 'string', 'max:255'],
            'fecNacimiento'=> ['required', 'date', 'max:255'],
            'usuario'=> ['required', 'string', 'max:255'],
            'email'=> ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        return User::create([
            'apePaterno'=>$input['apePaterno'],
            'apeMaterno'=>$input['apeMaterno'],
            'nombres'=>$input['nombres'],
            'fecNacimiento'=>$input['fecNacimiento'],
            'usuario'=>$input['usuario'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
    }
}
