<?php

namespace Database\Seeders;

use App\Models\Departamento;
use App\Models\Provincia;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProvinciaDistritoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $lima = Departamento::where('departamento', 'Lima')->first();

        $lima->provincia()->createMany([
            ['provincia' => 'Lima'],
            ['provincia' => 'Barranca'],
            // Puedes agregar más provincias según sea necesario
        ]);

        $limaProvincia = Provincia::where('provincia', 'Lima')->first();
        $limaProvincia->distrito()->createMany([
            ['distrito' => 'Lima'],
            ['distrito' => 'Miraflores'],
            // Puedes agregar más distritos según sea necesario
        ]);
    }
}
