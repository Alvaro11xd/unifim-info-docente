<?php

namespace Database\Seeders;

use App\Models\Pais;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // DB::transaction(function () {
        //     Pais::firstOrCreate(['pais'=> 'Perú']);
        //     Pais::firstOrCreate(['pais'=> 'Venezuela']);
        // });
        $peru = Pais::firstOrCreate(['pais' => 'Perú']);
        $venezuela = Pais::firstOrCreate(['pais' => 'Venezuela']);

        $peru->departamento()->createMany([
            ['departamento' => 'Lima'],
            ['departamento' => 'Arequipa'],
            ['departamento' => 'Trujillo'],
            ['departamento' => 'Piura'],
        ]);

        $venezuela->departamento()->createMany([
            ['departamento' => 'Caracas'],
            ['departamento' => 'Maracaibo'],
        ]);
    }
}
