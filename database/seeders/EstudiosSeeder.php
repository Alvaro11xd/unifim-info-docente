<?php

namespace Database\Seeders;

use App\Models\Estudios;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstudiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            Estudios::firstOrCreate(['estudios'=> 'Primaria']);
            Estudios::firstOrCreate(['estudios'=> 'Secundaria']);
            Estudios::firstOrCreate(['estudios'=> 'Técnico']);
            Estudios::firstOrCreate(['estudios'=> 'Profesional']);
            Estudios::firstOrCreate(['estudios'=> 'Licenciado']);
            Estudios::firstOrCreate(['estudios'=> 'Magister']);
            Estudios::firstOrCreate(['estudios'=> 'Doctorado']);
        });
    }
}
