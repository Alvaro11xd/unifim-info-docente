<?php

namespace Database\Seeders;

use App\Models\Parentesco;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentescoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            Parentesco::firstOrCreate(['parentesco'=> 'Hijo(a)']);
            Parentesco::firstOrCreate(['parentesco'=> 'Sobrino(a)']);
            Parentesco::firstOrCreate(['parentesco'=> 'Nieto(a)']);
            Parentesco::firstOrCreate(['parentesco'=> 'Hermano(a)']);
        });
    }
}
