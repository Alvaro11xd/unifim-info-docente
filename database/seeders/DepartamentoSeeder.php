<?php

namespace Database\Seeders;

use App\Models\Departamento;
use App\Models\Pais;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Departamento::create([
            'departamento' => 'Lima',
            'idPais' => Pais::where('pais', 'Perú')->first()->id,
        ]);

        Departamento::create([
            'departamento' => 'Caracas',
            'idPais' => Pais::where('pais', 'Venezuela')->first()->id,
        ]);
    }
}
