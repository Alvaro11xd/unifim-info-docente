<?php

namespace Database\Seeders;

use App\Models\EstadoCivil;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function(){
            EstadoCivil::firstOrCreate(['civil'=> 'Soltero(a)']);
            EstadoCivil::firstOrCreate(['civil'=> 'Casado(a)']);
        });
    }
}
