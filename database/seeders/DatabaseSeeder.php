<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\CategoriaLaboral;
use App\Models\Condicion;
use App\Models\DedicacionLaboral;
use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\Estudios;
use App\Models\Familiar;
use App\Models\FormacionProfesional;
use App\Models\Laboral;
use App\Models\Pais;
use App\Models\Parentesco;
use App\Models\Profesional;
use App\Models\Provincia;
use App\Models\RegimenPensionario;
use App\Models\Sexo;
use App\Models\Tipo;
use App\Models\TipoVia;
use App\Models\TipoZona;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Creando una carpeta para guardar las fotos en public/storage
        Storage::delete(Storage::allFiles("profile-photos"));
        Storage::deleteDirectory('profile-photos');
        Storage::makeDirectory('profile-photos');

        $this->call(EstadoCivilSeeder::class);
        $this->call(SexoSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(EstudiosSeeder::class);
        $this->call(ParentescoSeeder::class);
        Tipo::factory(5)->create();
        TipoVia::factory(6)->create();
        TipoZona::factory(7)->create();
        $this->call(PaisSeeder::class);
        // $this->call(DepartamentoSeeder::class);
        $this->call(ProvinciaDistritoSeeder::class);
        Condicion::factory(2)->create();
        CategoriaLaboral::factory(2)->create();
        DedicacionLaboral::factory(2)->create();
        RegimenPensionario::factory(3)->create();
        FormacionProfesional::factory(2)->create();
        // Domicilio::factory(8)->create();
        Familiar::factory(8)->create();
        Laboral::factory(8)->create();
        Profesional::factory(8)->create();
        // $this->call(DocenteSeeder::class);
    }
}
