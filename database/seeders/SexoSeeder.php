<?php

namespace Database\Seeders;

use App\Models\Sexo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            Sexo::firstOrCreate(['sexo'=> 'Masculino']);
            Sexo::firstOrCreate(['sexo'=> 'Femenino']);
        });
    }
}
