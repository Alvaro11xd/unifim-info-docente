<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DedicacionLaboral>
 */
class DedicacionLaboralFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $dedicacion = $this->faker->unique()->word;
        return [
            'dedicacion' => $dedicacion,
        ];
    }
}
