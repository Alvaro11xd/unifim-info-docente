<?php

namespace Database\Factories;

use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\Domicilio;
use App\Models\EstadoCivil;
use App\Models\Familiar;
use App\Models\Laboral;
use App\Models\Nacimiento;
use App\Models\Pais;
use App\Models\Profesional;
use App\Models\Provincia;
use App\Models\Sexo;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Docente>
 */
class DocenteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
