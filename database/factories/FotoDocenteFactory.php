<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FotoDocente>
 */
class FotoDocenteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'url'=> 'profile-photos/' . $this->faker->image('public/storage/profile-photos', 80, 80, null, false) // profile-photos/imagen.jpg
        ];
    }
}
