<?php

namespace Database\Factories;

use App\Models\FormacionProfesional;
use App\Models\Pais;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Profesional>
 */
class ProfesionalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $nombreArchivo = $this->faker->word;
        $extension = 'pdf';
        return [

            "idFormacion" => FormacionProfesional::all()->random()->id,
            "idPais" => Pais::all()->random()->id,
            "universidad" => $this->faker->word,
            "especialidad" => $this->faker->word,
            "fechaInicio" => $this->faker->date($format = "Y-m-d"),
            "fechaFinal" => $this->faker->date($format = "Y-m-d"),
            "grado" => $this->faker->word,
            "documento" => "$nombreArchivo.$extension",
            "observacion" => $this->faker->text(100),
        ];
    }
}
