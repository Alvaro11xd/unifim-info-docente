<?php

namespace Database\Factories;

use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\Pais;
use App\Models\Provincia;
use App\Models\Tipo;
use App\Models\TipoVia;
use App\Models\TipoZona;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Domicilio>
 */
class DomicilioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "idPais" => Pais::all()->random()->id,
            "idVia" => TipoVia::all()->random()->id,
            "nombreVia" => $this->faker->word,
            "idTipo" => Tipo::all()->random()->id,
            "numeroInmueble" => $this->faker->numberBetween(1, 1000),
            "idZona" => TipoZona::all()->random()->id,
            "nombreZona" => $this->faker->word,
            "idUser" => User::all()->random()->id,
        ];
    }
}
