<?php

namespace Database\Factories;

use App\Models\EstadoCivil;
use App\Models\Estudios;
use App\Models\Parentesco;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Familiar>
 */
class FamiliarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $nombreCompleto=$this->faker->name;
        $fechaNacimiento=$this->faker->date;
        $ocupacion=$this->faker->word;
        return [
            "nombreCompleto"=>$nombreCompleto,
            "idParentesco"=>Parentesco::all()->random()->id,
            "fechaNacimiento"=> $fechaNacimiento,
            "idCivil"=> EstadoCivil::all()->random()->id,
            "idEstudios"=> Estudios::all()->random()->id,
            "ocupacion"=> $ocupacion,
            "celular"=> $this->faker->phoneNumber,
            "dependiente"=> $this->faker->randomElement(['0','1'])
        ];
    }
}
