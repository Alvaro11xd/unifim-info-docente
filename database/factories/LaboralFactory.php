<?php

namespace Database\Factories;

use App\Models\CategoriaLaboral;
use App\Models\Condicion;
use App\Models\DedicacionLaboral;
use App\Models\RegimenPensionario;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Laboral>
 */
class LaboralFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "idCondicion" => Condicion::all()->random()->id,
            "idCategoria" => CategoriaLaboral::all()->random()->id,
            "idDedicacion" => DedicacionLaboral::all()->random()->id,
            "idRegimen" => RegimenPensionario::all()->random()->id,
            "horas" => $this->faker->numberBetween(1, 24),
            "horasLectivas" => $this->faker->numberBetween(1, 24),
            "direccionLaboral" => $this->faker->streetAddress,
            "fechaIngreso" => $this->faker->date($format = "Y-m-d", $max = "now"),
            "fechaNombramiento" => $this->faker->date($format = "Y-m-d", $max = "now"),
            "regimenPension" => $this->faker->word,
            "AFP" => $this->faker->word,
            "codAFP" => $this->faker->randomNumber(5),
            "codEsalud" => $this->faker->randomNumber(5),
            "otroSeguro" => $this->faker->word
        ];
    }
}
