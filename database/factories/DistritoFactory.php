<?php

namespace Database\Factories;

use App\Models\Provincia;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Distrito>
 */
class DistritoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $distrito = $this->faker->unique()->city;
        return [
            'distrito' => $distrito,
            'idProvincia' => Provincia::all()->random()->id
        ];
    }
}
