<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('laborals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idCondicion');
            $table->unsignedBigInteger('idCategoria');
            $table->unsignedBigInteger('idDedicacion');
            $table->unsignedBigInteger('idRegimen');
            $table->integer('horas');
            $table->integer('horasLectivas');
            $table->string('direccionLaboral', 50);
            $table->date('fechaIngreso')->default(now());
            $table->date('fechaNombramiento');
            $table->string('regimenPension', 30);
            $table->string('AFP', 30)->nullable();
            $table->string('codAFP', 30)->nullable();
            $table->string('codEsalud', 30)->nullable();
            $table->string('otroSeguro', 30)->nullable();
            $table->foreign('idCondicion')->references('id')->on('condicions')->onDelete('cascade');
            $table->foreign('idCategoria')->references('id')->on('categoria_laborals')->onDelete('cascade');
            $table->foreign('idDedicacion')->references('id')->on('dedicacion_laborals')->onDelete('cascade');
            $table->foreign('idRegimen')->references('id')->on('regimen_pensionarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('laborals');
    }
};
