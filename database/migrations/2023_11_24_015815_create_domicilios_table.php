<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idPais');
            $table->unsignedBigInteger('idVia');
            $table->string('nombreVia', 20);
            $table->unsignedBigInteger('idTipo');
            $table->unsignedBigInteger('numeroInmueble');
            $table->unsignedBigInteger('idZona');
            $table->string('nombreZona', 35);
            $table->unsignedBigInteger('idUser');

            $table->foreign('idPais')->references('id')->on('pais')->onDelete('cascade');
            $table->foreign('idVia')->references('id')->on('tipo_vias')->onDelete('cascade');
            $table->foreign('idTipo')->references('id')->on('tipos')->onDelete('cascade');
            $table->foreign('idZona')->references('id')->on('tipo_zonas')->onDelete('cascade');
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('domicilios');
    }
};
