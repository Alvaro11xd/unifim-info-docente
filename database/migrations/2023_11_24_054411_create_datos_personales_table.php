<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('datos_personales', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("idSexo");
            $table->string("depAcademico", 50)->nullable();
            $table->integer("DNI");
            $table->bigInteger("RUC");
            $table->string("libMilitar", 70)->nullable();
            $table->string("brevete", 70)->nullable();
            $table->string("vehivulo", 70)->nullable();
            $table->unsignedBigInteger("idCivil");
            $table->date("fechaMatrimonio")->nullable();
            $table->string("grupoSanguineo")->nullable();
            $table->string("telefono", 20);
            $table->string("celular", 20);
            $table->text("direccion");
            $table->unsignedBigInteger('idUser');

            $table->foreign("idSexo")->references("id")->on("sexos")->onDelete("cascade");
            $table->foreign("idCivil")->references("id")->on("estado_civils")->onDelete("cascade");
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('docentes');
    }
};
