<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profesionals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idFormacion');
            $table->unsignedBigInteger('idPais');
            $table->string('universidad', 40);
            $table->string('especialidad', 30);
            $table->date('fechaInicio');
            $table->date('fechaFinal');
            $table->string('grado', 30);
            $table->string('documento', 255)->nullable();
            $table->text('observacion')->nullable();

            $table->foreign('idFormacion')->references('id')->on('formacion_profesionals')->onDelete('cascade');
            $table->foreign('idPais')->references('id')->on('pais')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('profesionals');
    }
};
