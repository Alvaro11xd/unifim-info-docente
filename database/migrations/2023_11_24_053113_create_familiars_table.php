<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('familiars', function (Blueprint $table) {
            $table->id();
            $table->string("nombreCompleto", 50);
            $table->unsignedBigInteger("idParentesco");
            $table->date("fechaNacimiento");
            $table->unsignedBigInteger("idCivil");
            $table->unsignedBigInteger("idEstudios");
            $table->string("ocupacion", 30);
            $table->string("celular", 30);
            $table->enum("dependiente", [0, 1])->nullable();

            $table->foreign("idParentesco")->references("id")->on("parentescos")->onDelete("cascade");
            $table->foreign("idCivil")->references("id")->on("estado_civils")->onDelete("cascade");
            $table->foreign("idEstudios")->references("id")->on("estudios")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('familiars');
    }
};
