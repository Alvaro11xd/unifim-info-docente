<?php

use App\Http\Controllers\DatosPersonalesController;
use App\Http\Controllers\Docente\DomicilioController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/informacionDocente', function () {
        return view('contenido.index');
    })->name('contenido.index');
    Route::get('/estadoCivil', function () {
        return view('admin.estadoCivil');
    })->name('admin.estadoCivil');
    Route::get('/genero', function () {
        return view('admin.sexo');
    })->name('admin.sexo');
    Route::get('/pais', function () {
        return view('admin.pais');
    })->name('admin.pais');
    Route::get('/departamento', function () {
        return view('admin.departamento');
    })->name('admin.departamento');
    Route::get('/provincia', function () {
        return view('admin.provincia');
    })->name('admin.provincia');
    Route::get('/distrito', function () {
        return view('admin.distrito');
    })->name('admin.distrito');
    Route::get('/condicion', function () {
        return view('admin.condicion');
    })->name('admin.condicion');
    Route::get('/categoria', function () {
        return view('admin.categoria');
    })->name('admin.categoria');
    Route::get('/tiposVia', function () {
        return view('admin.tipoVia');
    })->name('admin.tipoVia');
    Route::get('/estudios', function () {
        return view('admin.estudio');
    })->name('admin.estudios');
    Route::get('/parentescos', function () {
        return view('admin.parentesco');
    })->name('admin.parentesco');
    Route::get('/tipos', function () {
        return view('admin.tipo');
    })->name('admin.tipo');
    Route::get('/tiposZona', function () {
        return view('admin.tipoZona');
    })->name('admin.tipoZona');
    Route::get('/dedicacionLaboral', function () {
        return view('admin.dedicacionLaboral');
    })->name('admin.dedicacionLaboral');
    Route::get('/regimenPensionario', function () {
        return view('admin.regimenPensionario');
    })->name('admin.regimenPensionario');
    Route::get('/formacionProfesional', function () {
        return view('admin.formacionProfesional');
    })->name('admin.formacionProfesional');
    Route::get('/usuarios', function () {
        return view('admin.usuarios');
    })->name('admin.usuarios');
    Route::resource('domicilios', DomicilioController::class)->names('docente.domicilios');
    Route::resource('datosPersonales', DatosPersonalesController::class)->names('docente.datosPersonales');
});
