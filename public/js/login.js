document.addEventListener("DOMContentLoaded", () => {
    const formLogin = document.querySelector("#formLogin");

    formLogin.addEventListener("submit", e => {
        e.preventDefault();
        const nombreUsuario = document.querySelector("#usuario").value;
        const contraseñaUsuario = document.querySelector("#contraseña").value;

        inciarSesion(nombreUsuario,contraseñaUsuario);
    })

    function inciarSesion(nombreUsuario,contraseñaUsuario) {
        fetch("./js/usuarios.json")
            .then(res => res.json())
            .then(data => {
                const usuarios = data;
                const usuarioEncontrado = usuarios.find(usuario => usuario.usuario === nombreUsuario && usuario.contraseña === contraseñaUsuario)

                if (usuarioEncontrado) {
                    window.location.replace("../contenido/inicio.html");
                } else {
                    alert("Credenciales incorrectas")
                }
            })
            .catch(error => {
                console.error("Error al cargar ususarios.json: ", error);
            })
    }
})