// VARIABLES GLOBALES
const listElements = document.querySelectorAll(".list_button--click"),
    sideMenu = document.querySelector("#side_menu"),
    btnOpen = document.querySelector("#btn_open"),
    body = document.querySelector("#body"),
    main = document.querySelector("#main"),
    overlay = document.querySelector("#overlay"),
    nav = document.querySelector("#nav"),
    modalOverlay = document.getElementById("modal_overlay_1"),
    modalOverlay2 = document.getElementById("modal_overlay_2"),
    modalOverlay3 = document.getElementById("modal_overlay_3"),
    modales = document.querySelectorAll(".ventana_contenedor"),
    btnAbrirModal = document.getElementById("btn_añadir_domicilio"),
    btnAbrirModal2 = document.getElementById("btn_añadir_familiar"),
    btnAbrirModal3 = document.getElementById("btn_añadir_formacion"),
    btnCerrarModal = document.querySelectorAll("#modal_btn_cerrar"),
    botonCerrarSesion = document.querySelector("#botonCerrarSesion");

// LÓGICA PARA LAS SECCIONES CON UN TAB
const tabs = document.querySelectorAll(".btn_tab"),
    all_content = document.querySelectorAll(".contenido");

tabs.forEach((tab, index) => {
    tab.addEventListener("click", (e) => {
        tabs.forEach((tab) => {
            tab.classList.remove("btn_tab-activo");
        });
        tab.classList.add("btn_tab-activo");
        all_content.forEach((contenido) => {
            contenido.classList.remove("btn_tab-activo");
            if (!contenido.classList.contains("btn_tab-activo")) {
                const $no_content = document.querySelector(".no_content");
                $no_content.innerHTML = "No hay contenido";
            }
        });
        if (all_content[index])
            all_content[index].classList.add("btn_tab-activo");
    });
});

/**
 * FUNCION QUE CONTROLA
 * TODA LA BARRA LATERAL Y
 * LA BARRA DE NAVEGACIÓN
 */
function setupBarraLateral() {
    // BARRA LATERAL Y BARRA DE NAVEGACION
    listElements.forEach((listElement) => {
        listElement.addEventListener("click", () => {
            listElement.classList.toggle("arrow");

            let height = 0;
            let menu = listElement.nextElementSibling; // hermano adyacente de list_button (list_show)
            //console.log(menu.scrollHeight) // alto minimo del elemento

            // Si el alto del menu es cero, es decir que está oculto, pues captura su alto minimo para modificar su altura y hacerlo visible
            if (menu.clientHeight == "0") {
                height = menu.scrollHeight;
            }
            menu.style.height = `${height}px`;
        });
    });

    // Ejecutando la funcion al hacer click en el menu
    btnOpen.addEventListener("click", open_close_menu);

    // Evento para ocultar el menú cuando se hace clic fuera del menú
    overlay.addEventListener("click", close_menu);

    // Evento para mostrar y ocultar el menu
    function open_close_menu() {
        body.classList.toggle("body_move");
        sideMenu.classList.toggle("menu_side_move");
        nav.classList.toggle("ancho_maximo");
        if (window.innerWidth < 760) {
            overlay.classList.add("visible");
            nav.classList.toggle("ancho_maximo");
        }
    }

    function close_menu() {
        body.classList.remove("body_move");
        sideMenu.classList.remove("menu_side_move");
        overlay.classList.remove("visible");
        // nav.classList.add("ancho_maximo");
    }

    // Si el ancho de la pagina es menor a 810, ocultará el menu al recargar la página
    if (window.innerWidth < 810) {
        body.classList.remove("body_move");
        sideMenu.classList.remove("menu_side_move");
        nav.classList.remove("ancho_maximo");
    }

    // Haciendo el menu responsive(adaptable)
    window.addEventListener("resize", () => {
        if (window.innerWidth > 760) {
            body.classList.add("body_move");
            sideMenu.classList.remove("menu_side_move");
            overlay.classList.remove("visible");
            nav.classList.remove("ancho_maximo");
            // main.classList.remove("relleno");
        }

        if (window.innerWidth < 760) {
            body.classList.remove("body_move");
            sideMenu.classList.remove("menu_side_move");
            overlay.classList.remove("visible");
            nav.classList.remove("ancho_maximo");
        }
    });
    // FIN BARRA LATERAL Y BARRA DE NAVEGACION
}

/**
 * FUNCION QUE CONTROLA
 * TODOS LOS MODALES EN GENERAL
 */
function setupVentanaModal() {
    // VENTANA MODAL
    if (btnAbrirModal || btnAbrirModal2 || btnAbrirModal3) {
        btnAbrirModal.addEventListener(
            "click",
            () => (modalOverlay.style.display = "flex")
        );
        btnAbrirModal2.addEventListener(
            "click",
            () => (modalOverlay2.style.display = "flex")
        );
        btnAbrirModal3.addEventListener(
            "click",
            () => (modalOverlay3.style.display = "flex")
        );
    }

    const cerrarModal = () => {
        modales.forEach((modal) => {
            modal.classList.add("cerrar_modal");
            setTimeout(() => {
                modal.classList.remove("cerrar_modal");
                modalOverlay.style.display = "none";
                modalOverlay2.style.display = "none";
                modalOverlay3.style.display = "none";
                modalFotos.style.display = "none";
            }, 1000);
        });
    };

    btnCerrarModal.forEach((btn_cerrar) => {
        btn_cerrar.addEventListener("click", () => cerrarModal());
    });

    window.addEventListener("click", (e) => {
        if (
            e.target == modalOverlay ||
            e.target == modalOverlay2 ||
            e.target == modalOverlay3 ||
            e.target == modalFotos
        )
            cerrarModal();
    });
    // FIN VENTANA MODAL
}

/**
 * FUNCION QUE CONTROLA
 * LAS CONFIGURACIONES DEL SISTEMA
 */
function setupMain() {
    // MAIN

    // CERRAR SESION
    if (botonCerrarSesion) {
        botonCerrarSesion.addEventListener("click", (e) => {
            e.preventDefault();
            Swal.fire({
                title: "Cerrar sesión",
                text: "¿Estás seguro de salir del sistema?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#224fb1",
                cancelButtonColor: "#e02525",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Cancelar",
                position: "top",
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(window.location.replace("../index.html"));
                }
            });
        });
    }
    // FIN MAIN
}

// EJECUTANDO LAS FUNCIONES CUANDO SE TERMINE DE CARGAR EL DOM
document.addEventListener("DOMContentLoaded", () => {
    setupBarraLateral();
    setupVentanaModal();
    setupMain();
});
