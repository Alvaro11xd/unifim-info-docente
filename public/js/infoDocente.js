// VARIABLES GLOBALES
// Variable para manejar la carga de las fotos del docente
let fotoDocente = document.querySelector("#fotoDocente"),
    file = document.querySelector("#inputFoto"),
    btnModalFotos = document.querySelector("#btnModalFotos"),
    modalFotos = document.querySelector("#modalFotos"),
    fotos = document.querySelector(".historial_fotos"),
    pdfSustentario = document.querySelector("#pdfSustentario"),
    modalPDF = document.querySelectorAll("#modalPDF");

// Variables para manejar la carga del PDF
const modalOverlay4 = document.getElementById("modal_overlay_4"),
    $modal4 = document.querySelector("#modal_4");

// FUNCION PARA MOSTRAR EL MODAL DE FOTOS
function mostrarModalFotos() {
    modalFotos.style.display = "flex";
}

// FUNCION PARA MOSTRAR LA FOTO ACTUALIZADA EN EL LOCAL STORAGE
function mostrarFotoLocal(imagen) {
    // Almacena la última imagen seleccionada en el localStorage
    localStorage.setItem("ultimaImagenSeleccionada", imagen);
    fotoDocente.src = imagen;
}

// FUNCION PARA MOSTRAR EL HISTORIAL DE FOTOS Y CARGAR LAS FOTOS GUARDADAS EN EL LOCAL STORAGE
function mostrarHistorialFotos() {
    document
        .querySelectorAll(".contenedorFoto")
        .forEach((info) => info.remove());
}

/**
 * FUNCION PARA CERRAR
 * EL MODAL DE HISTORIAL DE FOTOS Y
 * MODAL PDF
 */
function cerrarModalInfoDocente() {
    $modal4.classList.add("cerrar_modal");
    setTimeout(() => {
        $modal4.classList.remove("cerrar_modal");
        modalOverlay4.style.display = "none";
    }, 1000);
}

// EVENTOS
if (btnModalFotos) {
    btnModalFotos.addEventListener("click", mostrarModalFotos);
}
window.addEventListener("click", (e) => {
    if (e.target === modalOverlay4 || e.target === modalFotos)
        cerrarModalInfoDocente();
});

// EJECUTANDO EVENTOS
mostrarHistorialFotos();
